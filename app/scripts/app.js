/* globals FileAPI, io, $ */
(function () {
  'use strict';

  angular
    .module('masApp', [
      'toastr',
      'ui.router',
      'mas.unit',
      'mas.user',
      'masApp.hub',
      'masApp.equipment',
      'masApp.resource',
      'masApp.production',
      'masApp.bundle',
      'masApp.inventory',
      'masApp.container',
      'masApp.stock',
      'masApp.partner',
      'masApp.sensor'
    ])

    .run(function (BASE_URL, $rootScope, hubService, Auth, $state, toastr, Production, UnitFactory, MASConfig, AUTH_STATE_LOGIN, Equipment, $location) {
      $rootScope.connection = io.socket.isConnected();
      $rootScope.hub = {};
      var vm = this;

      function initiate() {
        io.socket.get('/socket/initiate', function (success) {
          $rootScope.connection = true;

          _.map(success.unit, function (eachUnit) {
            UnitFactory.addUnit(eachUnit);
          });
          _.map(success.config, function (config) {
            MASConfig.configs[config.key] = config.value;
          });
          var result = success.whoami;
          $rootScope.hub = success.hub;
          console.log(success);

          if (_.get(result, 'id') !== $rootScope.currentUser.id || $rootScope.currentUser.role.title !== _.get(result, 'role')) {
            if (_.get(result, 'role') !== 'public') {
              //로그아웃.
              Auth.logout(function () {
                $location.path('/login');
              });
            } else {
              console.log('만약 로컬에만 로그인 되어있다면');
              //만약 로컬에만 로그인 되어있다면
              Auth.logoutLocal(function () {
                $location.path('/login');
                // $window.location.reload(true);
              });
            }
          }
          $rootScope.$digest();
        });
      }

      $rootScope.$on('changeWorking', function () {
        console.log('changeWorking');
        initiate();
      });

      $rootScope.$on('forbidden', function () {
        if (Auth.isLoggedIn()) {
          initiate();
        }
      });


      $rootScope.$on('USER_CHANGED', function () {
        initiate();
      });

      $rootScope.$on('unauthorized', function () {
        if (Auth.isLoggedIn()) {
          initiate();
        }
      });

      $rootScope.$on('equipment:updated', function (event, data) {
        if (data.id === $rootScope.hub.id) {
          if (Auth.isLoggedIn()) {
            if ((data.data.operator !== $rootScope.hub.operator) || (data.data.targetResource !== $rootScope.hub.targetResource) || (data.data.operation !== $rootScope.hub.operation)) {
              // initiate();
              $rootScope.hub = data.data;
            }
          }
        }
      });

      io.socket.on('connect', function () {
        $rootScope.connection = true;
        initiate();
        $rootScope.$broadcast('SOCKET_CONNECT');
      });

      io.socket.on('reconnect', function () {
        $rootScope.connection = false;
        $rootScope.$digest();
      });
      // io.socket.on('sensor', function (data) {
      //   $rootScope.$broadcast('sensor:created', data);
      // });
      //
      io.socket.on('reboot', function (data) {
        ipc.send('reboot');
      });

      io.socket.on('remote', function (data) {
        // console.log(data);
        var port = data;
        ipc.send('remote', port);
      });


      io.socket.on('disconnect', function () {
        console.log('접속끊김');
        $rootScope.connection = false;
        // $rootScope.$digest();
        $state.go('public.disconnected');
        toastr.warning('서버 연결이 끊겼습니다');
      });

      // $rootScope.reloadState = function () {
      // 	$rootScope.$broadcast('production:reload');
      // };

    })
    .run(function ($rootScope, $state, Auth, ACCESS_LEVELS, AUTH_STATE_HOME, AUTH_STATE_LOGIN) {
      $rootScope.$on('$stateChangeStart', function (event, toState, toParams, fromState) {

        var targetAuth = (toState.hasOwnProperty('data')) ? toState.data.access : 'public';

        if (!Auth.authorize(targetAuth)) {
          console.log('Seems like you tried accessing a route you don\'t have access to...');

          $rootScope.error = 'Seems like you tried accessing a route you don\'t have access to...';
          event.preventDefault();

          if (fromState.url === '^') {
            if (Auth.isLoggedIn()) {
              $state.go(AUTH_STATE_HOME);
            } else {
              // $rootScope.error = null;
              $state.go(AUTH_STATE_LOGIN);
            }
          }
        }
      });
    })
    .run(function ($rootScope, toastr, ThrottleGenerator) {
      var ipc = require('electron').ipcRenderer;

      var tg = new ThrottleGenerator.ThrottleGenerator('id');

      function throttledBarcodeBroadcast(barcode) {
        tg.throttle('barcord', broadcastBarcode, 1000, {
          id: barcode
        }, [barcode]);
      }

      function broadcastBarcode(barcode) {
        $rootScope.$broadcast('barcode', barcode);
      }

      ipc.on('barcode', function (event, data) {
        throttledBarcodeBroadcast(data);
        // $rootScope.$broadcast('barcode', data);
      });

      ipc.on('production:start', function (event, data) {
        console.log('생산 시작');
        $rootScope.$broadcast('production:start', data);
      });

      ipc.on('production:finish', function (event, data) {
        console.log('생산 종료');
        $rootScope.$broadcast('production:finish', data);
      });

      ipc.on('production:endOnly', function (event, data) {
        console.log('신호');
        $rootScope.$broadcast('production:endOnly', data);
      });

    })
    .run(function ($injector, $rootScope, BASE_URL, MAS_C, MAS_H, $state, AUTH_STATE_HOME) {
      $rootScope.BASE_URL = BASE_URL;
      $rootScope.IMAGE_BASE_URL = BASE_URL;
      $rootScope.MAS_C = MAS_C;
      $rootScope.MAS_H = MAS_H;

      $state.go(AUTH_STATE_HOME);
    });
})();