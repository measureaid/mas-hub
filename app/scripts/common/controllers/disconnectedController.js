(function() {
  'use strict';
  /* globals io */
  angular
    .module('masApp')
    .controller('disconnectedController', disconnectedController);

  /* @ngInject */
  function disconnectedController($state, $rootScope, AUTH_STATE_HOME) {
    var vm = this;

    if ($rootScope.connection) {
      console.log('Connected');
      $state.go(AUTH_STATE_HOME);
    }

    io.socket.on('reconnect', function() {
      $state.go(AUTH_STATE_HOME);
    });

    io.socket.on('connect', function() {
      $state.go(AUTH_STATE_HOME);
    });

  }
})();