(function() {
  'use strict';

  angular
    .module('masApp')
    .controller('signinFormController', signinFormController);

  function signinFormController($rootScope, Auth, $state, toastr) {
    var vm = this;

    vm.loginForm = {};
    vm.login = {};

    if (Auth.isLoggedIn()) {
      $state.go('public.home');
    }

    vm.startLogin = function() {
      Auth.loginByBarcode(vm.login, function(res) {
        vm.login = {};
        toastr.info(res.name + ' 님 로그인 성공', '로그인');
        $state.go('public.home');
      }, function(err) {
        if (err.status === 404) {
          toastr.error('사용자를 찾을 수 없습니다.', '로그인 실패');
        } else {
          toastr.error(err.body.message, '로그인 실패');
        }
        vm.login = {};
        delete vm.login.barcode;
      });
    };
  }
})();