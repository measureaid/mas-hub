(function () {
  'use strict';
  /* globals _*/
  angular
    .module('masApp')
    .controller('topBarController', topBarController);


  /* @ngInject */
  function topBarController($rootScope, $scope, hubService, $window, Sensor) {
    var vm = this;

    $scope.hub = hubService.hub;

    // $scope.$on('sensor:created', function(event, data) {
    //   if (data.data.type === 'inputTemp') {
    //     $scope.in_temp = data.data.value;
    //   } else if (data.data.type === 'outputTemp') {
    //     $scope.out_temp = data.data.value;
    //   }
    // });
    getSensor();

    function getSensor() {
      setInterval(function () {
        console.log('sensor');
        Sensor.list({
          limit: 2,
          sort: 'createdAt DESC'
        }).$promise.then(function (sensor) {
          _.map(sensor.result, function (each) {
            if (each.type === 'inputTemp') {
              $scope.in_temp = each.value;
            } else if (each.type === 'outputTemp') {
              $scope.out_temp = each.value;
            }
          });
        });
      }, 60000);
    }

    /////////////////
  }
})();