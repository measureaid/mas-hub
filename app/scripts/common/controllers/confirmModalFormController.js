(function() {
	'use strict';

	angular
		.module('masApp')
		.controller('confirmModalFormCtrl', confirmModalFormCtrl);

	confirmModalFormCtrl.$inject = ['title', 'body', '$modalInstance'];

	/* @ngInject */
	function confirmModalFormCtrl(title, body, $modalInstance) {
		var vm = this;
		vm.title = title;
		vm.body = body;

		vm.ok = function() {
			$modalInstance.close(vm.production);
		};

		vm.cancel = function() {
			$modalInstance.dismiss('cancel');
		};
	}
})();
