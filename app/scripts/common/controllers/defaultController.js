/* globals _*/
'use strict';
(function() {

  angular
    .module('masApp')
    .controller('defaultController', defaultController);

  function defaultController($scope, $rootScope, $state, $timeout, hubService, Auth, toastr, Container, $window, ThrottleGenerator, Production) {
    var vm = this;
    var tg = new ThrottleGenerator.ThrottleGenerator('id');

    vm.barcode = '';
    vm.simulate = simulate;

    $scope.reload = reload;

    function reload() {
      $window.location.reload();
    }

    function simulate(barcode) {
      throttledBarcodeBroadcast(barcode);
    }

    function throttledBarcodeBroadcast(barcode) {
      tg.throttle('barcord', broadcastBarcode, 1000, {
        id: barcode
      }, [barcode]);
    }

    function broadcastBarcode(barcode) {
      $rootScope.$broadcast('barcode', barcode);
    }


    $scope.$on('production:start', function(event, data) {
      console.log('생산 시작');
      if (Auth.isLoggedIn()) {
        productionStart();
      } else {
        toastr.warning('로그인 되어 있지 않은 상태에서 생산을 진행하려 합니다.', '경고');
        // TODO: 비인가 생산 정보 저장
      }
    });
    $scope.$on('production:finish', function(event, data) {
      console.log('생산 완료');
      if (Auth.isLoggedIn()) {
        productionFinish();
      } else {
        toastr.warning('로그인 되어 있지 않은 상태에서 생산을 진행하려 합니다.', '경고');
        // TODO: 비인가 생산 정보 저장
      }
    });

    $scope.$on('production:endOnly', function(event, data) {
      console.log('생산 시작 + 완료');
      if (Auth.isLoggedIn()) {
        productionEndOnly();
      } else {
        toastr.warning('로그인 되어 있지 않은 상태에서 생산을 진행하려 합니다.', '경고');
        // TODO: 비인가 생산 정보 저장
      }
    });

    $scope.$on('recommand:force', function(event, data) {
      inject(data.barcode, data.force);
    });

    $scope.$on('barcode', function(event, data) {
      $scope.barcode = _.trim(data);
      var isLogin = _.startsWith($scope.barcode, 'SH');
      if (isLogin) {
        vm.startLogin({
          barcode: parseInt(_.trimStart($scope.barcode, 'SH'), 10)
        });
      } else {
        if (!Auth.isLoggedIn()) {
          return toastr.error('로그인을 먼저 해 주세요', '로그인 필요');
        }
      }


      var isProduction = _.startsWith($scope.barcode, 'P');
      if (isProduction) {
        assign($scope.barcode);
      }

      var isBundle = _.startsWith($scope.barcode, 'B');
      if (isBundle) {
        inject($scope.barcode);
      }

      var isContainer = _.startsWith($scope.barcode, 'C');
      if (isContainer) {
        Container.read({
          id: $scope.barcode
        }).$promise.then(function(result) {
          if (!_.isNull(result.collect_from) && !_.isUndefined(result.collect_from) && !_.isEmpty(result.collect_from)) {
            inject($scope.barcode);
          } else {
            collect($scope.barcode);
          }
        });
      }

    });

    vm.exitDevice = function() {
      const ipc = require('electron').ipcRenderer;
      ipc.send('shutdown');
    }

    vm.productionDone = function() {
      if (_.get($rootScope, 'production.parent')) {
        Production.update({
          id: _.get($rootScope, 'production.parent'),
          is_done: true
        }).$promise.then(function() {
          toastr.success('생산 완료되었습니다.');
        });
      } else {
        toastr.warn('생산이 할당되어 있지 않습니다.');
      }
    }

    vm.startLogin = function(data) {
      return Auth.loginByBarcode(data).then(
        function(res) {
          toastr.info(_.get(res, 'name'), '로그인 성공');
          $state.go('public.home');
        }).catch(
        function(err) {
          console.log(err);
          if (err.status === 404) {
            toastr.error('사용자를 찾을 수 없습니다.', '로그인 실패');
          } else {
            toastr.error(_.get(err, 'body.message'), '로그인 실패');
          }
        });
    };

    function assign(productionId) {
      return hubService.assign({
        production: productionId
      }).$promise.then(function(success) {
        toastr.success(productionId, '생산 할당');
        $rootScope.hub = success;
        $rootScope.$broadcast('hub:assign');

      }, function(err) {
        toastr.error(_.get(err, 'body.message'), '할당 실패');
      });
    }

    function unassign(productionId) {
      hubService.unassign({
        production: productionId
      }).$promise.then(function(success) {
        $rootScope.$broadcast('hub:unassign');

      }, function(err) {
        toastr.error(_.get(err, 'body.message'), '할당 해제 실패');
      });
    }

    function productionStart() {
      var hub = $rootScope.hub;

      if (!hub.operation) {
        return toastr.error('공정이 할당되어 있지 않습니다.', '생산 시작 실패');
      }
      if (!hub.targetResource) {
        return toastr.error('목표 생산 자원이 설정되어 있지 않습니다.', '생산 시작 실패');
      }

      return hubService.start({
        // resourceId: hub.targetResource,
        // operationId: hub.operation
      }).$promise.then(function(success) {
        $rootScope.$broadcast('hub:started');
      }, function(err) {
        toastr.error(_.get(err, 'body.message'), '생산 시작 실패');
      });
    }

    function productionFinish() {
      var hub = $rootScope.hub;

      if (!hub.operation) {
        return toastr.error('공정이 할당되어 있지 않습니다.', '생산 시작 실패');
      }
      if (!hub.targetResource) {
        return toastr.error('목표 생산 자원이 설정되어 있지 않습니다.', '생산 시작 실패');
      }

      return hubService.end({
        // resourceId: hub.targetResource,
        // operationId: hub.operation
      }).$promise.then(function(success) {
        $rootScope.$broadcast('hub:ended');
      }, function(err) {
        toastr.error(_.get(err, 'body.message'), '생산 종료 실패');
      });
    }

    function productionEndOnly() {
      var hub = $rootScope.hub;

      if (!hub.operation) {
        return toastr.error('공정이 할당되어 있지 않습니다.', '생산 시작 실패');
      }
      if (!hub.targetResource) {
        return toastr.error('목표 생산 자원이 설정되어 있지 않습니다.', '생산 시작 실패');
      }

      return hubService.endOnly({
        // resourceId: hub.targetResource,
        // operationId: hub.operation
      }).$promise.then(function(success) {

      }, function(err) {
        toastr.error(_.get(err, 'body.message'), '생산 시작 실패');
      });
    }

    function inject(id, force) {
      if (!_.isUndefined($rootScope.hub.operation_data) && !_.isNull($rootScope.hub.operation_data) && _.startsWith(id, 'C')) {
        if (!_.isUndefined($rootScope.hub.operation_data.mold) && !_.isNull($rootScope.hub.operation_data.mold) && _.startsWith(id, 'C')) {
          return toastr.error('사출에는 재료만 투입할 수 있습니다.', id);
        }
      }
      hubService.inject({
        id: id,
        force: force
      }).$promise.then(function(success) {
        $rootScope.$broadcast('hub:inject');

        toastr.success('투입이 완료되었습니다.', id);
        if ($rootScope.error) {
          delete $rootScope.error.recommend;
        }
      }, function(err) {
        if (err.body && typeof err.body == 'string') {
          try {
            err.body = JSON.parse(err.body);
          } catch (error) {

          }
        }
        if (err.body && err.body.hasOwnProperty('message') && _.get(err, 'body.message').hasOwnProperty('recommend')) {
          if (!$rootScope.error) {
            $rootScope.error = {};
          }
          $rootScope.error.barcode = id;
          $rootScope.error.recommend = _.get(err, 'body.message').recommend;
          toastr.error('선입선출이 지켜지지 않았습니다', '선입선출 에러');
        } else {
          toastr.error(_.get(err, 'body.message'), '투입 에러');
        }
      });
    }

    function collect(boxId) {
      if (_.get($rootScope.hub, 'operation_data.config.package')) {
        return toastr.error('검사 공정에서는 수집할 수 없습니다.');
      }
      if (_.startsWith(boxId, 'C')) {
        hubService.collect({
          box: boxId,
        }).$promise.then(function(success) {
          toastr.success('생산품 수집이 완료되었습니다.', boxId);
        }, function(err) {
          console.log(err);
          toastr.error(_.get(err, 'body.message'), '수집오류');
        });
      } else {
        toastr.error('공정용 상자에만 생산품을 수집할 수 있습니다.', '수집오류');
      }
    }


    $rootScope.style = 'style1';
    $rootScope.theme = 'blue-grey';
    $scope.data = {};
    $scope.effect = '';
    $scope.header = {
      form: false,
      chat: false,
      theme: false,
      footer: true,
      history: false,
      animation: '',
      boxed: '',
      layout_menu: '',
      theme_style: 'style1',
      header_topbar: 'header-fixed',
      menu_style: 'sidebar-collapsed',
      menu_collapse: 'sidebar-collapsed',
      layout_horizontal_menu: '',
      toggle: function(k) {
        switch (k) {
          case 'chat':
            $scope.header.chat = !$scope.header.chat;
            break;
          case 'form':
            $scope.header.form = !$scope.header.form;
            break;
          case 'sitebar':
            $scope.header.menu_style = $scope.header.menu_style ? '' : (($scope.header.layout_menu === '') ? 'sidebar-collapsed' : 'right-side-collapsed');
            break;
          case 'theme':
            $scope.header.theme = !$scope.header.theme;
            break;
          case 'history':
            $scope.header.history = !$scope.header.history;
            $scope.header.menu_style = $scope.header.history ? 'sidebar-collapsed' : 'sidebar-default';
            break;
        }
      },

      collapse: function(c) {
        if (c === 'change') {
          $scope.header.menu_collapse = '';
        } else {
          if ($scope.header.menu_style) {
            $scope.header.menu_style = '';
            $scope.header.menu_collapse = $scope.header.menu_collapse ? '' : 'sidebar-collapsed';
          } else {
            $scope.header.menu_collapse = $scope.header.menu_collapse ? '' : 'sidebar-collapsed';
          }
        }

      }
    };
  }

})();