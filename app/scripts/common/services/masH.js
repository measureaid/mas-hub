(function() {
  'use strict';

  var masH = {
    RESOURCE: {
      TYPE: {
        1: '재료',
        2: '반제품',
        3: '재공품',
        4: '제품',
        5: '상품',
        6: '장비'
      },
      SAFETY: {
        1: '충분',
        2: '부족'
      }
    },
    SCHEDULE: {
      TYPE: {
        30: '휴일',
        40: '휴가',
        50: '출장',
        60: '휴직',
        70: '기타'
      }
    },
    ISSUE: {
      STATUS: {
        202: '할 일',
        203: '하는 중',
        204: '완료'
      }
    },
    TIMEOFF: {
      TYPE: {
        10: '부여',
        21: '연차휴가',
        22: '유급휴가',
        23: '무급휴가',
        24: '외근',
        30: '청산'
      },
      SCHEDULE: {
        40: '전체',
        45: '오전반차',
        50: '오후반차',
      }
    },
    TEMPLATE: {
      TYPE: {
        1: '자원 템플릿',
        2: '문서 템플릿',
        3: '검사 템플릿',
        4: '결재 템플릿',
        5: '이슈 템플릿',
        6: '휴가 템플릿',
        7: '근태 템플릿',
        8: '프로젝트 템플릿'
      }
    },
    DOCUMENT: {
      TYPE: {
        1: '일반문서',
        2: '검사기록',
        3: '결재문서',
        4: '금형기록',
        5: '인사기록카드',
        6: '면담기록'

      },
      STAGE: {
        1: '초물',
        2: '중물',
        3: '종물'
      }
    },
    DEVICE: {
      TYPE: {
        81: '안드로이드',
        82: '아이폰',
        83: '웹'
      }
    },
    STATUS: {
      TYPE: {
        1: '생성됨',
        2: '업데이트됨',
        3: '삭제됨'
      }
    },
    PARTNER: {
      TYPE: {
        1: '바이어',
        2: '하청',
        3: 'AS파트너'
      }
    },
    CATEGORY: {
      TYPE: {
        1: '일반',
        2: '폴더',
        3: 'WMS'
      }
    },
    CONTAINER: {
      TYPE: {
        1: '실제창고',
        2: '임시창고',
        3: '장비공간',
        4: '공정용박스',
        5: '작업용공간',
        6: '랙'
      }
    },
    TASK: {
      STATUS: {
        1: 'TODO',
        2: 'DOING',
        3: 'DONE'
      }
    },
    PROGRESS: {
      STATUS: {
        1: '준비',
        2: '진행중',
        3: '완료'
      }
    },
    PRODUCTION: {
      STATUS: {
        1: '준비',
        2: '진행중',
        4: '진행중(대기)',
        3: '완료'
      },
      CORRECTION: {
        49: '해결',
        40: '부족',
        44: '과잉',
      },
      ACTION: {
        1: '투입',
        2: '시작',
        3: '종료',
        8: '신호',
        4: '불량',
        6: '수집',
        7: '포장',
        30: '업데이트',
        31: '신규',
        32: '추가',
        33: '환입',
        38: '분할'
      }
    },
    TASKINVENTORY: {
      STATUS: {
        1: 'TODO',
        2: 'DOING',
        3: 'DONE'
      },
      EVALUATION: {
        1: 'STARTED',
        2: 'NOT STARTED',
        3: 'FINISHED',
        4: 'NOT FINISHED',
        5: 'EMPTY'
      }
    },
    LOGISTICS: {
      TYPE: {
        1: '외부',
        2: '내부'
      }
    },
    ARRIVAL: {
      ACTION: {
        11: '새로 입고',
        12: '추가 입고'
      },
      STATUS: {
        1: 'INITIAL',
        2: 'ACCEPTED',
        3: 'REJECTED'
      }
    },
    WMS: {
      ACTION: {
        10: '생성',
        11: '입고',
        12: '생산',
        15: '부족',
        19: '획득',
        20: '제거',
        21: '출고',
        22: '사용',
        23: '폐기',
        28: '삭제',
        29: '손실',
        25: '해결',
        30: '이동',
        31: '분리',
        32: '병합',
        41: '변경',
        42: '포장'
      }
    },

    EQUIPMENT: {
      TYPE: {
        51: '생산장비',
        52: '도구',
        53: '허브',
        55: '바코드프린터'
      },
      STATUS: {
        1: '온라인',
        2: '작동중/사용중',
        4: '오프라인'
      }
    },
    ORDER: {
      TYPE: {
        81: 'OUTSIDE',
        82: 'INSIDE'
      },
      STATUS: {
        83: 'INITIAL',
        84: 'DISCUSS',
        85: 'ACTIVATE',
        86: 'COMPLETE'
      },
      DELIVER: {
        87: 'TODO',
        88: 'DOING',
        89: 'DONE'
      }
    },
    CONTRACT: {
      TYPE: {
        91: '수주',
        92: '발주'
      },
      STATUS: {
        93: '초기',
        94: '협의중',
        95: '진행중',
        96: '완료'
      }
    },
    APPROVAL: {
      STATUS: {
        1: '대기',
        2: '진행중',
        3: '완료',
        4: '반려'
      },
      LINE: {
        1: '팀 내부'
      }
    },
    VALUE: {
      TYPE: {
        1: '접수 유형',
        2: '고장 유형',
        3: '비용 유형',
        4: '알림',
        9: '기타'
      }
    },
    SUPPORT: {
      STATUS: {
        101: '접수',
        102: '대기',
        103: '처리중',
        104: '완료',
        105: '완결',
        106: '이관'
      }
    },
    MANAGE: {
      TYPE: {
        1: '판매',
        2: '구매'
      }
    },
    UNIT: {
      BASE: {
        'NONE': '없음',
        'LENGTH': '길이',
        'MASS': '질량',
        'TIME': '시간',
        'CURRENT': '전류',
        'TEMPERATURE': '온도',
        'LUMINOUS_INTENSITY': '밝기',
        'AMOUNT_OF_SUBSTANCE': '수량',
        'FORCE': '힘',
        'SURFACE': '표면적',
        'VOLUME': '부피',
        'ENERGY': '에너지',
        'POWER': '운동량',
        'ELECTRIC_CHARGE': '전하량',
        'ELECTRIC_CAPACITANCE': '캐피시턴스',
        'ELECTRIC_POTENTIAL': '포텐션',
        'ELECTRIC_RESISTANCE': '저항',
        'ELECTRIC_INDUCTANCE': '인덕턴스',
        'ELECTRIC_CONDUCTANCE': '컨덕턴스',
        'MAGNETIC_FLUX': '자속',
        'MAGNETIC_FLUX_DENSITY': '자속밀도',
        'FREQUENCY': '주파수',
        'ANGLE': '각도',
        'BIT': '비트',
        'PRESSURE': '압력'
      }
    }
  };

  angular
    .module('masApp')
    .constant('MAS_H', masH);
})();