(function() {
  /* globals _ */
  'use strict';
  angular
    .module('masApp')
    .factory('MASFactory', MASFactory);

  function MASFactory($q, MAS_C) {
    return {
      batch: batch,
      whitelist: whitelist,
      blacklist: blacklist,
      productable: productable,
      getKey: getKey,
      treeify: treeify,
      merge: merge,
      ceil: ceil,
      fileType: fileType,
      numberWithCommas: numberWithCommas,
      toggleInArray: toggleInArray,
      exists: exists,
      formatOnlyDate: function formatDate(date) {
        var d = new Date(date),
          month = '' + (d.getMonth() + 1),
          day = '' + d.getDate(),
          year = d.getFullYear();

        if (month.length < 2) month = '0' + month;
        if (day.length < 2) day = '0' + day;

        return [year, month, day].join('-');
      },
      getBetweenDate: getBetweenDate,
      urlBuilder: function encodeQueryData(data) {
        var ret = [];
        for (var d in data)
          ret.push(encodeURIComponent(d) + '=' + encodeURIComponent(data[d]));
        return ret.join('&');
      }
    };

    function exists(item, array, itemKey, key) {
      var query = {};
      if (!key) {
        key = 'id';
      }
      if (!itemKey) {
        itemKey = 'id';
      }
      query[key] = item[itemKey];

      var found = _.find(array, query);
      return !!found;
    }

    function toggleInArray(item, array, itemKey, key) {
      if (!key) {
        key = 'id';
      }
      if (!itemKey) {
        itemKey = 'id';
      }

      var query = {};
      query[key] = item[itemKey];

      var found = _.find(array, query);

      if (found) {
        _.remove(array, query);
      } else {
        var pushItem = {};
        pushItem[key] = item[itemKey];
        array.push(pushItem);
      }
    }


    function fileType(each_file) {
      var type = each_file.type.split('/');
      return type[0];
    }

    function getBetweenDate(startDate, endDate, name, interval) {
      interval = interval || 1;

      // logger.debug(name, startDate.toISOString(), startDate.toISOString());
      var retVal = [];
      var current = new Date(startDate);
      while (current < endDate) {
        retVal.push(_.clone(current));
        var nextDate = new Date(current);
        nextDate.setDate(current.getDate() + interval);
        current = nextDate;
      }
      return retVal;
    }

    function numberWithCommas(x) {
      return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ',');
    }

    function treeify(_list, idAttr, parentAttr, childrenAttr) {
      if (!idAttr) idAttr = 'id';
      if (!parentAttr) parentAttr = 'parent';
      if (!childrenAttr) childrenAttr = 'children';

      var list = angular.copy(_list);

      var treeList = [];
      var lookup = {};


      list.forEach(function(obj) {
        lookup[obj[idAttr]] = obj;
        obj[childrenAttr] = [];
      });
      list.forEach(function(obj) {
        if (obj[parentAttr] != null) {
          if (lookup[obj[parentAttr]]) {
            lookup[obj[parentAttr]][childrenAttr].push(obj);
          } else {
            treeList.push(obj);
          }
        } else {
          treeList.push(obj);
        }
      });
      return treeList;
    }

    function merge(item, list, key) {
      if (!key) {
        key = 'id';
      }
      var condition = {};
      condition[key] = item[key];
      var legacy = _.find(list,
        condition);
      if (legacy) {
        _.extend(legacy, _.omit(item, key));
      } else {
        list.push(item);
      }
    }
    ////

    function getKey(input, keyName) {
      var output;
      if (input) {
        if (_.isObject(input)) {
          if (input.hasOwnProperty(keyName)) {
            output = input[keyName];
          }
        } else {
          output = input;
        }
      }
      return output;
    }

    function ceil(number, increment, offset) {
      return Math.ceil((number - offset) / increment) * increment + offset;
    }

    function productable(type) {
      if (type instanceof Array) {
        return true;
      }

      var isProductable = false;

      switch (type) {
        case MAS_C.RESOURCE.TYPE.HALF_PRODUCT:
          isProductable = true;
          break;
        case MAS_C.RESOURCE.TYPE.WORK_IN_PROGRESS:
          isProductable = true;
          break;
        case MAS_C.RESOURCE.TYPE.PRODUCT:
          isProductable = true;
          break;
        default:
          isProductable = false;
      }
      return isProductable;
    }

    function batch(jobs, processWithPromise) {
      var promises = jobs.map(function(job) {
        if (!job.$batchResolved) {
          return processWithPromise(job);
        }
      });
      // console.log(promises);
      return $q.all(promises);
    }

    function blacklist(inputObject, blacklist) {
      // logger.debug(blacklist);
      if (!_.isObject(inputObject) || _.isArray(inputObject)) {
        return -1;
      }

      function matchRule(str, rule) {
        return new RegExp('^' + rule.split('*').join('.*') + '$').test(str);
      }

      var ret = {};
      _.map(inputObject, function(each_object, each_key) {
        // logger.debug('BLACK', each_key);
        // logger.debug(!_.includes(blacklist, each_key));
        var match = false;
        _.map(blacklist, function(blackitem) {
          if (matchRule(each_key, blackitem)) {
            match = true;
          }
        });
        if (!match) {
          ret[each_key] = each_object;
        }
      });

      return ret;
    }

    function whitelist(inputObject, whiteList) {
      // logger.debug(whiteList);
      if (!_.isObject(inputObject) || _.isArray(inputObject)) {
        return -1;
      }
      var ret = {};
      _.map(inputObject, function(eachObject, eachKey) {
        // logger.debug('BLACK', eachKey);
        // logger.debug(!_.includes(whiteList, eachKey));
        if (_.includes(whiteList, eachKey)) {
          // return true;
          ret[eachKey] = eachObject;
        }
      });
      return ret;
    }
  }
})();