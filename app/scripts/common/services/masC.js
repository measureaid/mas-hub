(function() {
  'use strict';
  var masC = {
    RESOURCE: {
      TYPE: {
        MATERIAL: 1,
        HALF_PRODUCT: 2,
        WORK_IN_PROGRESS: 3,
        PRODUCT: 4,
        GOODS: 5,
        EQUIPMENT: 6
      },
      SAFETY: {
        ENOUGH: 1,
        LACK: 2
      }
    },
    SCHEDULE: {
      TYPE: {
        HOLIDAY: 'HOLIDAY',
        VACATION: 'VACATION',
        TRIP: 'TRIP',
        REST: 'REST',
        ETC: 'ETC'
      }
    },
    ISSUE: {
      STATUS: {
        // INITIAL: 201,
        TODO: 202,
        DOING: 203,
        DONE: 209,
        // TALLYING: 204,
        // COMPLETE: 210,
        // REJECTED: 206,
        // SUSPENDED: 207
      }
    },
    TIMEOFF: {
      TYPE: {
        SET: 10,
        VACATION: 21,
        PAID: 22,
        UNPAID: 23,
        OUTSIDE: 24,
        SETTLE: 30
      },
      SCHEDULE: {
        FULL: 'FULL',
        MORNINGHALF: 'MORNINGHALF',
        AFTERNOONHALF: 'AFTERNOONHALF',
      }
    },
    TEMPLATE: {
      TYPE: {
        RESOURCE: 1,
        DOCUMENT: 2,
        INSPECTION: 3,
        APPROVAL: 4,
        ISSUE: 5,
        TIMEOFF: 6,
        PUNCTUALITY: 7,
        PROJECT: 8,
      }
    },
    DOCUMENT: {
      TYPE: {
        NORMAL: 1,
        INSPECTION_RECORD: 2,
        APPROVAL: 3,
        MOLD: 4,
        PERSONNEL: 5,
        INTERVIEW: 6
      },
      STAGE: {
        FIRST: 1,
        MID: 2,
        LAST: 3
      }
    },
    DEVICE: {
      TYPE: {
        ANDROID: 81,
        IOS: 82,
        WEB: 83
      }
    },
    STATUS: {
      TYPE: {
        CREATED: 1,
        UPDATED: 2,
        DESTROYED: 4
      }
    },
    PARTNER: {
      TYPE: {
        BUYER: 1,
        SUBCONTRACTOR: 2,
        SUPPORTER: 3
      }
    },
    CATEGORY: {
      TYPE: {
        NORMAL: 1,
        FOLDER: 2,
        WMS: 3
      }
    },
    CONTAINER: {
      TYPE: {
        REAL: 1,
        TEMP: 2,
        EQUIPMENT: 3,
        OPERATION: 4,
        WORKPLACE: 5,
        RACK: 6
      }
    },
    TASK: {
      STATUS: {
        TODO: 1,
        DOING: 2,
        DONE: 3
      }
    },
    PROGRESS: {
      STATUS: {
        INITIAL: 1,
        PROGRESS: 2,
        COMPLETE: 3
      }
    },
    PRODUCTION: {
      STATUS: {
        INITIAL: 1,
        PROGRESS: 2,
        WAIT: 3,
        COMPLETE: 4
      },
      CORRECTION: {
        SETTLE: 49,
        SHORTAGE: 40,
        EXCESS: 44,
      },
      ACTION: {
        INJECT: 1,
        START: 2,
        END: 3,
        ENDONLY: 8,
        DEFECT: 4,
        COLLECT: 6,
        PACKAGE: 7,
        UPDATE_STOCK: 30,
        NEW_STOCK: 31,
        ADD_STOCK: 32,
        SPLIT_STOCK: 38
      }
    },
    TASKINVENTORY: {
      STATUS: {
        TODO: 1,
        DOING: 2,
        DONE: 3
      },
      EVALUATION: {
        STARTED: 1,
        NOTSTARTED: 2,
        FINISHED: 3,
        NOTFINISHED: 4,
        EMPTY: 5
      }
    },
    LOGISTICS: {
      TYPE: {
        EXTERNAL: 1,
        INTERNAL: 2
      }
    },
    ARRIVAL: {
      ACTION: {
        CREATE: 11,
        ENTER_STOCK: 12
      },
      STATUS: {
        INITIAL: 1,
        ACCEPTED: 2,
        REJECTED: 3
      }
    },
    WMS: {
      ACTION: {
        CREATE: 10,
        ENTER: 11,
        PRODUCTION: 12,
        SHORTAGE: 15,
        ACQUISITION: 19,
        VANISH: 20,
        DELIVER: 21,
        CONSUME: 22,
        JUNK: 23,
        DELETE: 28,
        LOSS: 29,
        SETTLEMENT: 25,
        MOVE: 30,
        SPLIT: 31,
        MERGE: 32,
        RETURN: 33,
        SHIFT: 41,
        PACKAGE: 42
      }
    },

    EQUIPMENT: {
      TYPE: {
        MACHINE: 51,
        TOOL: 52,
        HUB: 53,
        PRINTER: 55
      },
      STATUS: {
        IDLE: 1,
        WORKING: 2,
        OFFLINE: 4
      }
    },
    ORDER: {
      TYPE: {
        OUTSIDE: 81,
        INSIDE: 82
      },
      STATUS: {
        INITIAL: 83,
        DISCUSS: 84,
        ACTIVATE: 85,
        COMPLETE: 86
      },
      DELIVER: {
        TODO: 87,
        DOING: 88,
        DONE: 89
      }
    },
    CONTRACT: {
      TYPE: {
        OUTSIDE: 91,
        INSIDE: 92
      },
      STATUS: {
        INITIAL: 93,
        DISCUSS: 94,
        ACTIVATE: 95,
        COMPLETE: 96
      }
    },
    APPROVAL: {
      STATUS: {
        INITIAL: 1,
        PROGRESS: 2,
        ACCEPTED: 3,
        REJECTED: 4
      },
      LINE: {
        TEAM_INTERNAL: 1
      }
    },
    VALUE: {
      TYPE: {
        SUPPORT_TYPE: 1,
        SUPPORT_PROBLEM: 2,
        SUPPORT_EXPENSE: 3,
        NOTIFICATION: 4,
        ALERT: 5
      }
    },
    SUPPORT: {
      STATUS: {
        INITIAL: 101,
        WAIT: 102,
        PROGRESS: 103,
        DONE: 104,
        COMPLETE: 105,
        TRANSFER: 106
      }
    },
    MANAGE: {
      TYPE: {
        SELL: 1,
        BUY: 2
      }
    },
    UNIT: {
      BASE: {
        'NONE': 'NONE',
        'LENGTH': 'LENGTH',
        'MASS': 'MASS',
        'TIME': 'TIME',
        'CURRENT': 'CURRENT',
        'TEMPERATURE': 'TEMPERATURE',
        'LUMINOUS_INTENSITY': 'LUMINOUS_INTENSITY',
        'AMOUNT_OF_SUBSTANCE': 'AMOUNT_OF_SUBSTANCE',
        'FORCE': 'FORCE',
        'SURFACE': 'SURFACE',
        'VOLUME': 'VOLUME',
        'ENERGY': 'ENERGY',
        'POWER': 'POWER',
        'ELECTRIC_CHARGE': 'ELECTRIC_CHARGE',
        'ELECTRIC_CAPACITANCE': 'ELECTRIC_CAPACITANCE',
        'ELECTRIC_POTENTIAL': 'ELECTRIC_POTENTIAL',
        'ELECTRIC_RESISTANCE': 'ELECTRIC_RESISTANCE',
        'ELECTRIC_INDUCTANCE': 'ELECTRIC_INDUCTANCE',
        'ELECTRIC_CONDUCTANCE': 'ELECTRIC_CONDUCTANCE',
        'MAGNETIC_FLUX': 'MAGNETIC_FLUX',
        'MAGNETIC_FLUX_DENSITY': 'MAGNETIC_FLUX_DENSITY',
        'FREQUENCY': 'FREQUENCY',
        'ANGLE': 'ANGLE',
        'BIT': 'BIT',
        'PRESSURE': 'PRESSURE'
      }
    }
  };

  angular
    .module('masApp')
    .constant('MAS_C', masC);
})();