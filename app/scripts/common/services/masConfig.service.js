(function () {
	'use strict';
	/* global _ */
	angular
		.module('masApp')
		.service('MASConfig', Config);
	/* @ngInject */
	function Config($rootScope, localStorageService, masResource) {

		var vm = this;
		var S = masResource('config', {
			// Arrival
			list: {
				method: 'GET'
			},
			read: {
				method: 'GET'
			}
		});


		// Config Config Table
		// vm.keys = [{
		// 	name: 'defaultMoveContainer',
		// 	key: 'bc-1'
		// }, {
		// 	name: 'defaultContainer',
		// 	key: 'bc-2'
		// }, {
		// 	name: 'lastCaliper',
		// 	key: 'bc-3'
		// }, {
		// 	name: 'lastBarcodeReader',
		// 	key: 'bc-4'
		// }];

		vm.configs = getConfigsFromLocalStorage(vm.keys);

		// add a listener on a key
		$rootScope.$watch(angular.bind(vm, function () {
			return vm.configs;
		}), function (newValue, oldValue) {
			// console.log(newValue);
			_.map(newValue, function (value, key) {
				//Validation & Sanitize
				// console.log(value, key);
				if((oldValue && oldValue[key] !== newValue[key]) || !oldValue) {
					var sanitizedValue = validateConfigValue(key, value, vm.keys);
					if(sanitizedValue !== value) {
						// console.log(sanitizedValue, value);
						vm.configs[key] = oldValue[key];
						setConfigFromLocalStorage(key, value, vm.keys);
					} else {
						// This is called after the key "key" has changed, a good idea is to broadcast a message that key has changed
						// console.log(key, value);
						setConfigFromLocalStorage(key, value, vm.keys);
					}
				}
			});
		}, true);



		function getConfigsFromLocalStorage(configKeyMap) {
			var ret = {};
			_.map(configKeyMap, function (eachConfig) {
				ret[eachConfig.name] = getConfigFromLocalStorage(eachConfig.name, configKeyMap);
			});
			return ret;
		}

		function getConfigFromLocalStorage(name, conditions) {
			var keySetting = _.find(conditions, {
				name: name
			});
			if(keySetting && keySetting.hasOwnProperty('key') && typeof (keySetting.key) !== undefined) {
				return localStorageService.get(keySetting.key);
			} else {
				return;
			}
		}

		function setConfigFromLocalStorage(name, value, conditions) {
			// console.log(name, value);

			var keySetting = _.find(conditions, {
				name: name
			});
			if(!keySetting) {
				keySetting = {};
				keySetting.key = name;
			}
			if(getConfigFromLocalStorage(name, conditions) !== value) {
				localStorageService.set(keySetting.key, value);
				$rootScope.$broadcast('mas:config:changed', name, value);
			}

		}

		function validateConfigValue(key, value, conditions) {
			var condition = _.find(conditions, {
				key: key
			});
			// console.log(key);

			if(condition && condition.hasOwnProperty('enum')) {
				return validateEnum(value, condition.enum);
			}
			return value;
		}

		function validateEnum(value, conditions) {
			//키중에 있는지 검색, 없으면 첫번째 키 리턴
			if(conditions && conditions.hasOwnProperty(value)) {
				return value;
			} else if(conditions.hasOwnProperty(value)) {
				return Object.keys(conditions)[0];
			} else {
				return value;
			}
		}


		S.configs = vm.configs;
		return S;


	}
})();
