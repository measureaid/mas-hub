(function() {
  'use strict';

  angular
    .module('masApp')
    .service('ThrottleGenerator', service);

  function service() {
    this.ThrottleGenerator = ThrottleGenerator;
  }

  function ThrottleGenerator(key) {

    var self = this;

    if (!key) {
      self.key = 'id';
    } else {
      self.key = key;
    }
    self.nowThrottling = {};
    self.nowThrottlingTimeouts = {};


    ThrottleGenerator.prototype.throttle = function(type, func, delay, obj, arg, options) {
      var self = this;
      // var _ = require('lodash');
      if (!options) {
        options = {};
      }

      var keyValue = obj[self.key];
      if (!self.nowThrottlingTimeouts[type]) {
        self.nowThrottlingTimeouts[type] = {};
      }
      var timer = self.nowThrottlingTimeouts[type][keyValue];
      if (timer) {
        clearTimeout(timer); //cancel the previous timer.
        timer = null;
      }

      timer = setTimeout(function() {
        (function(type, k) {
          delete self.nowThrottling[type][k];
          delete self.nowThrottlingTimeouts[type][k];
        })(type, keyValue);
      }, 5000);

      if (!self.nowThrottling[type]) {
        self.nowThrottling[type] = {};
      }

      if (!self.nowThrottling[type][keyValue]) {
        self.nowThrottling[type][keyValue] = _.throttle(function(arg) {
          func.apply(undefined, arg);
        }, delay, options);
      }

      self.nowThrottling[type][keyValue](arg);
    };

  }
})();