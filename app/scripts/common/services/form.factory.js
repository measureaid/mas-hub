(function() {
  'use strict';
  /* globals _ */
  angular
    .module('masApp')
    .factory('FormFactory', FormFactory);

  /* @ngInject */
  function FormFactory() {
    var service = {
      merge: merge,
      addOrDelete: addOrDelete
    };

    function addOrDelete(input, name, where) {
      if (!(typeof input === 'undefined' || input instanceof Array && input.length === 0)) {
        where[name] = input;
      } else {
        if (where.hasOwnProperty(name)) {
          delete where[name];
        }
      }
    }

    function merge(item, list, key) {
      if (!key) {
        key = 'id';
      }
      var condition = {};
      condition[key] = item[key];
      var legacy = _.find(list,
        condition);
      if (legacy) {
        _.extend(legacy, _.omit(item, key));
      } else {
        list.push(item);
      }
    }

    return service;
  }
})();