(function() {
  'use strict';
  /* globals io*/

  var forEach = angular.forEach,
    copy = angular.copy,
    extend = angular.extend,
    isObject = angular.isObject,
    isArray = angular.isArray,
    isFunction = angular.isFunction;


  angular
    .module('masApp')
    .provider('masResource', function() {

      var DEFAULT_CONFIGURATION = {
        // Set a route prefix, such as '/api'
        prefix: '',
        // When verbose, socket updates go to the console
        verbose: true,
        // Set a specific websocket
        socket: null,
        // Set a specific origin
        origin: null,
        // Set resource primary key
        primaryKey: 'id'
      };

      // window.onbeforeunload = function () {
      // 	if(io.socket) {
      // 		io.socket.disconnect();
      // 	}
      // };

      // io.socket.on('reconnecting', function (timeDisconnected, reconnectCount) {
      // 	$rootScope.$emit(MESSAGES.reconnecting, {
      // 		timeDisconnected: timeDisconnected,
      // 		reconnectCount: reconnectCount
      // 	});
      // });

      this.configuration = {};

      this.$get = ['$rootScope', '$window', '$log', '$q', function($rootScope, $window, $log, $q) {
        var config = extend({}, DEFAULT_CONFIGURATION, this.configuration);
        return socketFactory($rootScope, $window, $log, $q, config);
      }];
    });


  function socketFactory($rootScope, $window, $log, $q, config) {

    var DEFAULT_ACTIONS = {
      'get': {
        method: 'GET'
      },
      'save': {
        method: 'POST'
      },
      'query': {
        method: 'GET',
        isArray: true
      },
      'remove': {
        method: 'DELETE'
      },
      'delete': {
        method: 'DELETE'
      }
    };

    var MESSAGES = {
      // Resource
      created: '$masResourceCreated',
      updated: '$masResourceUpdated',
      destroyed: '$masResourceDestroyed',
      messaged: '$masResourceMessaged',
      addedTo: '$masResourceAddedTo',
      removedFrom: '$masResourceRemovedFrom',
      // Socket
      connected: '$sailsConnected',
      disconnected: '$sailsDisconnected',
      reconnected: '$sailsReconnected',
      reconnecting: '$sailsReconnecting',
      socketError: '$sailsSocketError'
    };


    return function(model, actions, options) {
      var registerEvents = function(model) {
        // if(io.socket.isConnected()) {
        console.debug('SUBSCRIBE on ' + model);

        io.socket.on(model, function(message) {

          if (options.verbose) {
            $log.info('masResource received \'' + model + '\' message: ', message);
          }
          switch (message.verb) {
            case 'updated':
              socketUpdateResource(message);
              break;
            case 'created':
              socketCreateResource(message);
              break;
            case 'destroyed':
              socketDeleteResource(message);
              break;
            case 'messaged':
              break;
            case 'addedTo':
              break;
            case 'removedFrom':
              break;
            default:
              break;
          }
          $rootScope.$broadcast(model + ':' + message.verb, extend({
            model: model
          }, message));
        });
        // }
      };

      if (typeof model != 'string' || model.length == 0) {
        throw 'Model name is required';
      }

      model = model.toLowerCase(); // Sails always sends models lowercase
      actions = extend({}, DEFAULT_ACTIONS, actions);
      options = extend({}, config, options);

      // Ensure prefix starts with forward slash
      if (options.prefix && options.prefix.charAt(0) != '/') {
        options.prefix = '/' + options.prefix;
      }


      // Setup socket default messages
      if (io.socket.isConnected()) {
        registerEvents(model);
      } else {
        io.socket.on('connect', function() {
          registerEvents(model);
        });
      }
      // // Disconnect socket when window unloads
      // Caching
      var cache = {};
      // TODO implement cache clearing?

      function removeFromCache(id) {
        delete cache[id];
        // remove this item in all known lists
        forEach(cache, function(cacheItem) {
          if (isArray(cacheItem)) {
            var foundIndex = null;
            forEach(cacheItem, function(item, index) {
              if (item[options.primaryKey] == id) {
                foundIndex = index;
              }
            });
            if (foundIndex != null) {
              cacheItem.splice(foundIndex, 1);
            }
          }
        });
      }

      // Resource constructor
      function Resource(value) {
        copy(value || {}, this);
      }

      function mergeParams(params, actionParams) {
        return extend({}, actionParams || {}, params || {});
      }

      // Handle a request
      // Does a small amount of preparation of data and directs to the appropriate request handler
      function handleRequest(item, params, action, success, error) {
        // When params is a function, it's actually a callback and no params were provided

        if (isFunction(params)) {
          error = success;
          success = params;
          params = {};
        }


        var instanceParams,
          actionParams = action && angular.isObject(action.params) ? action.params : {};
        if (action.method.toUpperCase() == 'GET') {

          instanceParams = mergeParams(params, actionParams);

          // Do not cache if:
          // 1) action is set to cache=false (the default is true) OR
          // 2) action uses a custom url (Sails only sends updates to ids) OR
          // 3) the resource is an individual item without an id (Sails only sends updates to ids)

          if (!action.cache || action.url || (!action.isArray && (!instanceParams || !instanceParams[options.primaryKey]))) { // uncached
            item = action.isArray ? [] : new Resource();
          } else {
            // cache key is 1) stringified params for lists or 2) id for individual items
            var key = action.isArray ? angular.toJson(instanceParams || {}) : instanceParams[options.primaryKey];
            // pull out of cache if available, otherwise create new instance
            item = cache[key] || (action.isArray ? []
              // Set key on object using options.primaryKey
              :
              (function() {
                var tmp = {};
                tmp[options.primaryKey] = key;
                return new Resource(tmp);
              })());
            cache[key] = item; // store item in cache
          }

          return retrieveResource(item, instanceParams, action, success, error);
        } else {
          // When we have no item, params is assumed to be the item data
          if (!item) {
            if (!isArray(params)) {
              item = new Resource(params);
              params = {};
            }
          }
          if (!isArray(params)) {
            instanceParams = mergeParams(params, actionParams);
          } else {
            item = copy(params);
            instanceParams = {};
          }

          if (action.method.toUpperCase() == 'POST' || action.method.toUpperCase() == 'PUT') { // Update individual instance of model
            return createOrUpdateResource(item, instanceParams, action, success, error);
          } else if (action.method.toUpperCase() == 'DELETE') { // Delete individual instance of model
            return deleteResource(item, instanceParams, action, success, error);
          }
        }
      }

      // Handle a response
      function handleResponse(item, response, action, deferred, delegate) {
        var body = response.body;
        action = action || {};

        item.$resolved = true;

        if (response && response.statusCode >= 400) {
          $log.error(response, item);

          if (response.statusCode === 401) {
            $rootScope.$broadcast('unauthorized');
            console.log('Response unauthorized 401');
          }
          if (response.statusCode === 403) {
            $rootScope.$broadcast('forbidden');
            console.log('Response forbidden 403');
          }

          deferred.reject(response, item, body);
        }
        // console.log(item, body);
        // else if(!isArray(item) && isArray(body) && body.length != 1) {
        // 	// This scenario occurs when GET is done without an id and Sails returns an array. Since the cached
        // 	// item is not an array, only one item should be found or an error is thrown.
        // 	var errorMessage = (body.length ? 'Multiple' : 'No') +
        // 		' items found while performing GET on a singular \'' + model + '\' Resource; did you mean to do a query?';
        //
        // 	$log.error(errorMessage);
        // 	deferred.reject(errorMessage, item, body);
        // }
        else {
          // converting single array to single item
          if (!isArray(item) && isArray(body)) {
            body = body[0];
          }
          if (isArray(action.transformResponse)) {
            forEach(action.transformResponse, function(transformResponse) {
              if (isFunction(transformResponse)) {
                body = transformResponse(body);
              }
            });
          }
          if (isFunction(action.transformResponse)) {
            body = action.transformResponse(body);
          }
          if (isFunction(delegate)) {
            delegate(body);
          }
          // 1) Internally resolve with both item and header getter
          // for pass'em to explicit success handler
          // 2) In attachPromise() cut off header getter, so that
          // implicit success handlers receive only item
          deferred.resolve({
            item: item,
            getHeaderFn: function(name) {
              return response && response.headers && response.headers[name];
            }
          });
        }
      }

      function attachPromise(item, success, error) {
        var deferred = $q.defer();
        item.$promise = deferred.promise.then(function(result) {
          // Like in ngResource explicit success handler
          // (passed directly as an argument of action call)
          // receives two arguments:
          // 1) item and 2) header getter function.

          (success || angular.noop)(result.item, result.getHeaderFn);

          // Implicit success handlers (bound via Promise API, .then())
          // receive only item argument
          return $q.when(result.item);
        });
        item.$promise.catch(error);
        item.$resolved = false;
        return deferred;
      }

      // Request handler function for GETs
      function retrieveResource(item, params, action, success, error) {
        var deferred = attachPromise(item, success, error);

        var url = buildUrl(model, params, action, params, options);
        item.$retrieveUrl = url;

        if (options.verbose) {
          $log.info('masResource calling GET ' + url);
        }

        io.socket.get(url, function(resData, response) {
          handleResponse(item, response, action, deferred, function(data) {
            if (isArray(item)) { // empty the list and update with returned data
              while (item.length) item.pop();
              forEach(data, function(responseItem) {
                responseItem = new Resource(responseItem);
                responseItem.$resolved = true;
                item.push(responseItem); // update list
              });
            } else {
              extend(item, data); // update item
              // If item is not in the cache based on its id, add it now
              if (!cache[item[options.primaryKey]]) {
                cache[item[options.primaryKey]] = item;
              }
            }
          });
        });
        return item;
      }

      // Request handler function for PUTs and POSTs
      function createOrUpdateResource(item, params, action, success, error) {
        var deferred = attachPromise(item, success, error);

        // prep data
        var transformedData;
        if (isFunction(action.transformRequest)) {
          var tmp = action.transformRequest(item);
          transformedData = angular.isObject(tmp) ? tmp : angular.fromJson(tmp);
        }

        // prevents prototype functions being sent
        var data;
        if (isArray(item)) {
          data = item;
        } else {
          data = copyAndClear(transformedData || item, {});
        }


        var url = buildUrl(model, data, action, params, options);
        // when Resource has id use PUT, otherwise use POST
        var method = (action) ? action.method : undefined;

        if (!method) {
          method = item[options.primaryKey] ? 'put' : 'post';
        }
        if (options.verbose) {
          $log.info('masResource calling ' + method.toUpperCase() + ' ' + url);
        }

        io.socket[method.toLowerCase()](url, data, function(response, jwr) {
          handleResponse(item, jwr, action, deferred, function(data) {
            extend(item, data);

            var message = {
              model: model,
              data: item
            };
            message[options.primaryKey] = item[options.primaryKey];

            if (method.toUpperCase() === 'PUT') {
              // Update cache
              socketUpdateResource(message);
              // // Emit event
              // $rootScope.$broadcast(MESSAGES.updated, message);
            } else {
              // Update cache
              socketCreateResource(message);
              // // Emit event
              // $rootScope.$broadcast(MESSAGES.created, message);
            }
          });
        });

        return item;
      }

      // Request handler function for DELETEs
      function deleteResource(item, params, action, success, error) {
        var deferred = attachPromise(item, success, error);
        var url = buildUrl(model, item, action, params, options);

        if (options.verbose) {
          $log.info('masResource calling DELETE ' + url);
        }
        io.socket.delete(url, function(resData, response) {
          handleResponse(item, response, action, deferred, function() {
            removeFromCache(item[options.primaryKey]);
            var tmp = {
              model: model
            };
            tmp[options.primaryKey] = item[options.primaryKey];
            $rootScope.$broadcast(MESSAGES.destroyed, tmp);
            // leave local instance unmodified
          });
        });

        return item;
      }

      function socketUpdateResource(message) {
        forEach(cache, function(cacheItem, key) {
          if (isArray(cacheItem)) {
            forEach(cacheItem, function(item) {
              if (item[options.primaryKey] == message[options.primaryKey]) {
                if (needsPopulate(message.data, item)) { // go to server for updated data
                  var tmp = {};
                  tmp[options.primaryKey] = item[options.primaryKey];
                  retrieveResource(item, tmp);
                } else {
                  extend(item, message.data);
                }
              }
            });
          } else if (key == message[options.primaryKey]) {
            if (needsPopulate(message.data, cacheItem)) { // go to server for updated data
              var tmp = {};
              tmp[options.primaryKey] = cacheItem[options.primaryKey];
              retrieveResource(cacheItem, tmp);
            } else {
              extend(cacheItem, message.data);
            }
          }
        });
      }

      function socketCreateResource(message) {
        // if(isArray(message)){
        //
        // }else{
        //
        // }
        if (!isArray(message.data)) {
          cache[message[options.primaryKey]] = new Resource(message.data);
          // when a new item is created we have no way of knowing if it belongs in a cached list,
          // this necessitates doing a server fetch on all known lists
          // TODO does this make sense?
          forEach(cache, function(cacheItem, key) {
            if (isArray(cacheItem)) {
              retrieveResource(cacheItem, angular.fromJson(key));
            }
          });
        } else {
          _.map(message.data, function(data) {

            cache[data[options.primaryKey]] = new Resource(data);
            // when a new item is created we have no way of knowing if it belongs in a cached list,
            // this necessitates doing a server fetch on all known lists
            // TODO does this make sense?
            forEach(cache, function(cacheItem, key) {
              if (isArray(cacheItem)) {
                retrieveResource(cacheItem, angular.fromJson(key));
              }
            });
          });
        }
      }

      function socketDeleteResource(message) {
        removeFromCache(message[options.primaryKey]);
      }

      // Add each action to the Resource and/or its prototype
      forEach(actions, function(action, name) {
        // fill in default action options
        action = extend({}, action);

        function actionMethod(params, success, error) {

          var self = this;
          if (action.fetchAfterReconnect) {
            // let angular-resource-sails refetch important data after
            // a server disconnect then reconnect happens
            io.socket.on('reconnect', function() {
              handleRequest(isObject(self) ? self : null, params, action, success, error);
            });
          }

          return handleRequest(isObject(this) ? this : null, params, action, success, error);
        }

        if (/^(POST|PUT|PATCH|DELETE)$/i.test(action.method)) {
          // Add to instance methods to prototype with $ prefix, GET methods not included
          Resource.prototype['$' + name] = actionMethod;
        }

        // All method types added to service without $ prefix
        Resource[name] = actionMethod;

      });

      // Handy function for converting a Resource into plain JSON data
      Resource.prototype.toJSON = function() {
        var data = extend({}, this);
        delete data.$promise;
        delete data.$resolved;
        return data;
      };


      return Resource;
    };
  }

  function needsPopulate(src, dst) {
    for (var key in src) {
      if (src.hasOwnProperty(key) && isObject(dst[key]) && !isObject(src[key])) {
        return true;
      }
    }
    return false;
  }

  /**
   * Deep copies and removes view properties
   */
  function copyAndClear(src, dst) {
    dst = dst || (isArray(src) ? [] : {});

    forEach(dst, function(value, key) {
      delete dst[key];
    });

    for (var key in src) {
      if (src.hasOwnProperty(key) && key.charAt(0) !== '$') {
        var prop = src[key];
        dst[key] = isObject(prop) ? copyAndClear(prop) : prop;
      }
    }
    return dst;
  }

  /**
   * Builds a sails URL!
   */
  function buildUrl(model, paramData, action, params, options) {
    var url = [];
    var urlParams = {};

    if (action && action.url) {
      var actionUrl = action.url;

      // Look for :params in url and replace with params we have
      var matches = action.url.match(/(:\w+)/g);

      if (matches) {
        forEach(matches, function(match) {
          var paramName = match.replace(':', '');
          if (paramName === options.primaryKey) {
            if (!isArray(paramData)) {
              actionUrl = actionUrl.replace(match, (paramData[options.primaryKey]) ? paramData[options.primaryKey] : '');
            }
          } else {
            if (!isArray(paramData)) {
              urlParams[paramName] = true;
              actionUrl = actionUrl.replace(match, (paramData[paramName]) ? paramData[paramName] : '');
            }
          }
        });
      }
      if (!_.isUndefined(actionUrl)) {
        actionUrl = actionUrl.replace('//', '/');
        url.push(actionUrl);
      }
    } else {
      url.push(options.prefix);
      url.push('/');
      url.push(model);
      if (paramData[options.primaryKey]) {
        url.push('/' + paramData[options.primaryKey]);
      }
    }

    var queryParams = {};

    angular.forEach(params, function(value, key) {
      if (!urlParams[key]) {
        queryParams[key] = (value) ? value : '';
      }
    });

    url.push(createQueryString(queryParams, options));
    return url.join('');
  }

  /**
   * Create a query-string out of a set of parameters, similar to way AngularJS does (as of 1.3.15)
   * @see https://github.com/angular/angular.js/commit/6c8464ad14dd308349f632245c1a064c9aae242a#diff-748e0a1e1a7db3458d5f95d59d7e16c9L1142
   */
  function createQueryString(params) {
    if (!params) {
      return '';
    }

    var parts = [];
    Object.keys(params).sort().forEach(function(key) {
      var value = params[key];
      if (key === 'id') {
        return;
      }
      if (value === null || angular.isUndefined(value)) {
        return;
      }
      if (!angular.isArray(value)) {
        value = [value];
      }
      value.forEach(function(v) {
        if (angular.isObject(v)) {
          v = angular.isDate(v) ? v.toISOString() : angular.toJson(v);
        }
        if (v) {
          parts.push(key + '=' + v);
        }
      });
    });
    return parts.length ? '?' + parts.join('&') : '';
  }
})();