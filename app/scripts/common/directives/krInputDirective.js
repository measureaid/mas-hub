(function () {
	'use strict';

	angular
		.module('masApp')

	.directive('krInput', ['$parse', function ($parse) {
		return {
			priority: 2,
			restrict: 'A',
			compile: function (element) {
				element.on('compositionstart', function (e) {
					e.stopImmediatePropagation();
					e.triggerHandler('compositionend');
				});
			},
		};
	}]);

})();
