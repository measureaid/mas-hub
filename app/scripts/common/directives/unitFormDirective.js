(function() {
    'use strict';

    angular
        .module('masApp')
        .directive('unitForm', unitFormDirective);

    function unitFormDirective() {
        var unitFormDirective = {
            restrict: 'EA',
            templateUrl: 'unit/templates/form.html',
            scope: {
                open: '='
            },
            link: linkFunc,
            controller: unitFormController,
            controllerAs: 'vm',
            bindToController: true
        };

        return unitFormDirective;

        function linkFunc(scope, el, attr, ctrl) {

        }
    }

    unitFormController.$inject = ['masApp'];

    function unitFormController(masApp) {
        var vm = this;

        vm.submit = function() {
            if ($scope.unit) {
                masApp.createUnit(vm.unit, function(success) {

                }, function(err) {

                });
            } else {
                masApp.createUnit(vm.unit, function(success) {

                }, function(err) {

                });
            }
        }

        activate();

        function activate() {

        }
    }
})();
