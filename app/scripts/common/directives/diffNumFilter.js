(function () {
	'use strict';

	angular
		.module('masApp')
		.filter('diffNum', diffNum);

	function diffNum() {
		return diffNumFilter;

		function diffNumFilter(original, changed) {
			var sign = '';
			if(changed - original > 0) {
				sign = '+';
			} else if(changed - original < 0) {
				sign = '-';
			}

			return sign + (changed - original);
		}
	}
})();
