/* globals _ */
(function() {
  'use strict';

  angular
    .module('masApp')
    .directive('breadcrumb', breadcrumb);

  /* @ngInject */
  function breadcrumb() {
    var directive = {
      restrict: 'EA',
      templateUrl: 'templates/common/breadcrumb.html',
      scope: {
        title: '=',
        navigation: '='
      },
      controller: breadcrumbController,
      controllerAs: 'vm',
    };

    return directive;

  }

  breadcrumbController.$inject = ['$scope'];

  function breadcrumbController($scope) {
    var vm = this;
    vm.isLast = function(title) {
      return _.last(vm.navigation).title === title;
    };
    activate();

    function activate() {

    }
  }
})();