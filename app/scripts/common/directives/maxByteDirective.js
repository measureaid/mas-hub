(function () {
	'use strict';

	angular
		.module('masApp')
		.filter('maxByte', maxByte);

	function maxByte() {
		return maxByteFilter;

		function maxByteFilter(params, option) {
			var cut = function (str, len) {
				var l = 0;
				for(var i = 0; i < str.length; i++) {
					l += (str.charCodeAt(i) > 128) ? 2 : 1;
					if(l > len) {
						return str.substring(0, i) + "...";
					}
				}
				return str;
			};

			/**
			 * bool String::bytes(void)
			 * 해당스트링의 바이트단위 길이를 리턴합니다. (기존의 length 속성은 2바이트 문자를 한글자로 간주합니다)
			 */
			var bytes = function (str) {
				var l = 0;
				for(var i = 0; i < str.length; i++) {
					l += (str.charCodeAt(i) > 128) ? 2 : 1;
				}
				return l;
			};
			if(params) {
				return cut(params, option);
			} else {
				return params;
			}
		}
	}
})();
