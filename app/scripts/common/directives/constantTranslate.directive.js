(function() {
  'use strict';
  /* global _ */
  angular
    .module('masApp')
    .filter('constantTranslate', constantTranslate);

  function constantTranslate(MAS_H) {



    return function constantTranslateFilter(value, type) {

      if (type && typeof(value) !== undefined) {
        // console.log(type, value);
        var options = type.split('.');
        // console.log(options);
        var now = MAS_H;
        _.map(options, function(eachOption) {
          // console.log(now);
          now = now[eachOption];
        });
        if (now) {
          var result = now[value];
          return result;
          // console.log('이거 되긴 되냐', vm.result);
        } else {
          return value;
        }
      }
    };

  }
})();