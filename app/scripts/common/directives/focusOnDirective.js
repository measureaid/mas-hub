(function () {
	'use strict';

	angular
		.module('masApp')
		.directive('focusOn', function ($rootScope) {
			return function (scope, elem, attr) {
				// console.log(attr.focusOn);
				$rootScope.$on(attr.focusOn, function (e) {
					elem[0].focus();
				});
			};
		});
})();
