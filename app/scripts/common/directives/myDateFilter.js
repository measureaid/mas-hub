(function () {
  'use strict';

  angular
    .module('masApp')
    .filter('myDate', function ($filter) {
      var angularDateFilter = $filter('date');
      return function (theDate, option) {
        if (theDate) {
          if (option === 'simple') {
            return angularDateFilter(theDate, 'yy/MM/dd');
          } else if (option === 'date') {
            return angularDateFilter(theDate, 'dd');
          } else if (option === 'half') {
            var temp = new Date(theDate).getHours();
            if (8 <= temp && temp <= 20) {
              return angularDateFilter(theDate, 'yy/MM/dd') + ' 주간';
            } else {
              return angularDateFilter(theDate, 'yy/MM/dd') + ' 야간';
            }
          } else {
            return angularDateFilter(theDate, 'yy/MM/dd HH:mm');
          }
        }
      };
    });
})();