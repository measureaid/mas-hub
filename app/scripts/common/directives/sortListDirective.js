(function() {
    'use strict';

    angular
        .module('masApp')
        .directive('sortList', SortListDirective);

    function SortListDirective() {
        var directive = {
            restrict: 'A',
            transclude: true,
            template: '<a ng-click="onClick()">' +
                '<span ng-transclude></span>' +
                '<i class="glyphicon" ng-class="{\'glyphicon-sort-by-alphabet\' : order === by && !reverse,  \'glyphicon-sort-by-alphabet-alt\' : order===by && reverse}"></i>' +
                '</a>',
            scope: {
                order: '=sortList',
                by: '@',
                reverse: '=',
                onConditionChange: '&?'
            },
            link: function(scope, element, attrs) {
                scope.onClick = function() {
                    scope.by = scope.order;
                    scope.reverse = !!!scope.reverse;

                    if (scope.onConditionChange) {
                        scope.onConditionChange()({
                            order: scope.by,
                            reverse: scope.reverse
                        });
                    }
                };
            }
        };

        return directive;
    }
})();
