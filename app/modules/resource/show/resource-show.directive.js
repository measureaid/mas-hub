(function() {
  'use strict';
  /* globals _ */
  angular
    .module('masApp.resource')
    .directive('resourceShow', resourceShow);

  /* @ngInject */
  function resourceShow() {
    var directive = {
      restrict: 'EA',
      templateUrl: 'modules/resource/show/resource-show.tmpl.html',
      scope: {
        input: '=resourceShow',
        resourceName: '=?',
        resource: '=?',
        badge: '=?',
        noClick: '=?'
      },
      controller: Controller,
      controllerAs: 'vm',
      bindToController: true
    };

    return directive;

  }

  function Controller(Resource, $scope, $rootScope, $element) {
    var vm = this;

    if (_.isUndefined(vm.badge)) {
      vm.badge = true;
    }
    vm.isVisible = true;

    $scope.$watch(angular.bind(vm, function() {
      return vm.input;
    }), function(newValue, oldValue) {
      activate();
    });

    $scope.$on('resource:updated', function(event, data) {
      if (vm.resource) {
        if (vm.resource.id === data.id) {
          _.merge(vm.resource, data.data);
          // console.log(vm.resource);
          angular.element(window).triggerHandler('resize');
        }
      }
    });

    function activate() {
      if (vm.isVisible) {
        if (_.isObject(vm.input) && vm.input.hasOwnProperty('id') && vm.input.id) {
          vm.resource = vm.input;
        } else if (vm.resourceName) {
          vm.resource = {
            id: vm.resource,
            name: vm.resourceName
          };
        } else {
          if (vm.input) {
            Resource.read({
              id: vm.input
            }).$promise.then(function(resource) {
              vm.resource = resource;
            });
          }
        }
      }
    }
  }
})();