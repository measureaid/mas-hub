(function() {
  'use strict';

  angular
    .module('masApp.resource')
    .service('Resource', resourceService);

  resourceService.$inject = ['masResource'];

  function resourceService(masResource) {
    var S = masResource('resource', {
      // Resource
      list: {
        method: 'GET',
        cache: false
        // isArray: true,
      },
      read: {
        cache: true,
        method: 'GET',
      }
    });
    return S;
  }
})();
