(function() {
  'use strict';

  angular
    .module('masApp.resource', [
      'ui.router',
      'ngFitText'
    ]);

})();