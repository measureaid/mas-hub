(function() {
    'use strict';

    angular
        .module('masApp.bundle')
        .service('Bundle', Bundle);

    Bundle.$inject = ['masResource'];

    function Bundle(masResource) {
        // console.log('BUNDLE SERVICE LOADED')
        var S = masResource('bundle', {
            // Bundle
            list: {
                method: 'GET',
            },
            read: {
                method: 'GET',
            },
            enter: {
                method: 'POST',
            },
            acquisition: {
                method: 'PUT',
                isArray: true,
                url: '/bundle/:id/acquisition'
            },
            consume: {
                method: 'PUT',
                isArray: true,
                url: '/bundle/:id/consume'
            },
            move: {
                method: 'PUT',
                url: '/bundle/:id/move'
            },
            loss: {
                method: 'PUT',
                url: '/bundle/:id/loss',
                isArray: true
            },
            junk: {
                method: 'PUT',
                url: '/bundle/:id/junk',
                isArray: true
            },
            deliver: {
                method: 'PUT',
                url: '/bundle/:id/deliver',
                isArray: true
            },
            recommend: {
                method: 'PUT',
                url: '/bundle/:id/recommend'
            },
            log: {
                method: 'GET',
                url: '/bundle/log'
            },
            traceFloat: {
                method: 'GET',
                url: '/bundle/traceFloat'
            },
        });
        return S;
    }
})();