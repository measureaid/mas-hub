(function() {
  'use strict';

  angular
    .module('masApp.bundle')
    .controller('HubBundlePrintController', HubBundlePrintController);

  function HubBundlePrintController($rootScope, $scope, Production, hubService, MAS_C) {
    var vm = this;

    if (_.isNull(vm.bundles) || _.isUndefined(vm.bundles) || _.isEmpty(vm.bundles)) {
      vm.bundles = [];
    }

    $rootScope.$watch('hub.id', function(newValue) {
      if (newValue && $rootScope.hub.operation_data.config &&
        $rootScope.hub.operation_data.config.package) {
        Production.findActionLog({
          sort: 'createdAt DESC',
          limit: '30',
          where: {
            hub: $rootScope.hub.id,
            type: MAS_C.PRODUCTION.ACTION.PACKAGE
          }
        }).$promise.then(function(bundles) {
          vm.bundles = bundles.result;
        });
      }
    });

    $scope.print = function(id) {
      hubService.print({
        data: {
          bundle: id
        }
      });
    };

  }
})();