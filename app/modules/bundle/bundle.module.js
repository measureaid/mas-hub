(function() {
  'use strict';

  angular
    .module('masApp.bundle', [
      'ui.router',
    ]);

})();