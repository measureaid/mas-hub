(function () {
	'use strict';

	angular
		.module('masApp.partner')
		.service('Partner', partnerService);

	partnerService.$inject = ['masResource'];

	function partnerService(masResource) {
		var S = masResource('partner', {
			list: {
				method: 'GET',
			},
			create: {
				method: 'POST',
			},
			read: {
				method: 'GET',
			},
			update: {
				method: 'PUT',
			},
			delete: {
				method: 'DELETE',
				isArray: true,
			},

			listCharge: {
				method: 'GET',
				isArray: true,
				url: '/partner/:id/charge',
			},
			createCharge: {
				method: 'POST',
				url: '/partner/:partner/charge',
			},
			readCharge: {
				method: 'GET',
				cache: true,
				url: '/partner/:id/charge',
			},
			deleteCharge: {
				method: 'DELETE',
				url: '/partner/:partner/charge/:charge',
			},
			createManage: {
				method: 'POST',
				url: '/partner/:id/manage',
			},
			readManage: {
				method: 'GET',
				cache: true,
				url: '/partner/:id/manage',
			},
			deleteManage: {
				method: 'DELETE',
				url: '/partner/:id/manage',
			},
		});

		return S;
	}
})();
