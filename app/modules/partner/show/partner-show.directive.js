(function () {
	'use strict';
	/* globals _ */
	angular
		.module('masApp.partner')
		.directive('partnerShow', partnerShow);

	/* @ngInject */
	function partnerShow() {
		var directive = {
			restrict: 'EA',
			template: '<span>{{vm.partner.name}}</span>',
			scope: {
				input: '=partnerShow'
			},
			controller: Controller,
			controllerAs: 'vm',
			bindToController: true
		};

		return directive;

	}

	function Controller(Partner, $scope) {
		var vm = this;

		$scope.$watch(angular.bind(vm, function () {
			return vm.input;
		}), function (newValue, oldValue) {
			activate();
		});

		function activate() {
			// console.log(vm.input);
			if (_.isObject(vm.input) && vm.input.hasOwnProperty('id') && vm.input.id) {
				vm.partner = _.clone(vm.input);
			} else {
				if (vm.input) {
					Partner.read({
						id: vm.input
					}).$promise.then(function (partner) {
						vm.partner = partner;
					});
				}
			}

		}
	}
})();
