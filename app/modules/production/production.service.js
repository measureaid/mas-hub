(function () {
  'use strict';

  angular
    .module('masApp.production')
    .service('Production', productionService);

  function productionService(masResource) {
    var S = masResource('production', {
      list: {
        method: 'GET',
      },
      read: {
        method: 'GET',
        url: '/production/:id'
      },
      update: {
        method: 'PUT',
        url: '/production/:id'
      },
      // start: {
      //   method: 'PUT',
      //   url: '/resource/:resourceId/operation/:operationId/start',
      //   isArray: true
      // },
      // end: {
      //   method: 'PUT',
      //   url: '/resource/:resourceId/operation/:operationId/end',
      //   isArray: true
      // },
      compensate: {
        method: 'PUT',
        url: '/production/:id/compensate',
      },
      // package: {
      //   method: 'PUT',
      //   url: '/resource/:resourceId/production/:productionId/package',
      //   isArray: true
      // }
      findActionLog: {
        method: 'GET',
        url: '/productionactionlog/'
      },
    });
    return S;
  }
})();