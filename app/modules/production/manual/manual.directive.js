(function() {
  'use strict';

  angular
    .module('masApp.production')
    .directive('manualButton', manualButton);

  /* @ngInject */
  function manualButton() {
    var directive = {
      restrict: 'EA',
      templateUrl: 'modules/production/manual/manual.tmpl.html',
      scope: {

      },
      controller: Controller,
      controllerAs: 'vm',
      bindToController: true
    };

    return directive;
  }

  /* @ngInject */
  function Controller($rootScope) {
    var vm = this;

    vm.start = function() {
      $rootScope.$broadcast('production:start');
    };

    vm.end = function() {
      $rootScope.$broadcast('production:finish');
    };

    vm.endOnly = function() {
      $rootScope.$broadcast('production:endOnly');
    };
  }
})();