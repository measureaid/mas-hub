(function() {
  'use strict';

  angular
    .module('masApp.production')
    .service('ProductionStatus', ProductionStatusService);

  function ProductionStatusService(masResource) {
    var S = masResource('productionstatus', {
      list: {
        method: 'GET',
      },
      read: {
        method: 'GET',
      }
    });
    return S;
  }
})();
