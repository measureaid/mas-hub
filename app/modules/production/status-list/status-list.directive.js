(function () {
  'use strict';
  /* globals _ */
  angular
    .module('masApp.production')
    .directive('productionStatusList', productionStatusList);

  /* @ngInject */
  function productionStatusList() {
    var directive = {
      restrict: 'EA',
      templateUrl: 'modules/production/status-list/status-list.tmpl.html',
      scope: {
        selected: '=?selectedProduction',
        productionstatuss: '=?statused',
        // getProductionStatus: '&?reload'
        // selectedResource: '=?selectedResource',
        // selectedOperation: '=?selectedOperation'
      },
      controller: Controller,
      controllerAs: 'vm',
      bindToController: true
    };

    return directive;
  }


  /* @ngInject */
  function Controller(ProductionAssign, $rootScope, $scope, hubService) {
    var vm = this;

    vm.change = change;
    vm.containerCapacityOrPackgeQuantity = 0;
    vm.priority = priority;
    vm.class = [];

    vm.unassign = function (l) {
      return hubService.unassign({
        production: l.productionIds
      });
    };

    getProductionStatus();

    vm.selectedCss = function (item) {
      if (item.resource.id === $rootScope.hub.targetResource || item.resource.id === $rootScope.hub.target_resource_data.wip) {
        return 'hover';
      } else {
        return null;
      }
    };

    function getProductionStatus() {
      hubService.status().$promise.then(function (success) {
        vm.list = success;
        if (_.isUndefined(vm.selected) || _.isEmpty(vm.selected) || _.isNull(vm.selected)) {
          var selectedProductionStatus = _.find(vm.list, function (each) {
            if (each.resource.id == $rootScope.hub.targetResource) {
              return true;
            }
          });
          vm.change(selectedProductionStatus);
        }
      });
    }

    function priority(l) {
      var sort = _.sortBy(l.bundle, 'parentProduction');
      if ($rootScope.hub.targetResource !== l.resource.id) {
        return;
      }
      if (l.parentProduction.length > 1) {
        vm.class[l.resource.id] = 'color-gray';
      } else {
        $rootScope.remainderQuantity = 0;
        vm.class[l.resource.id] = null;
        return;
      }
      var ret = _.filter(l.bundle, function (bundle) {
        if (_.first(sort).parentProduction == bundle.parentProduction) {
          return true;
        }
      });
      $rootScope.remainderQuantity = angular.copy(_.sumBy(ret, 'quantity'));
      return $rootScope.remainderQuantity;
    }
    var debounced = _.debounce(getProductionStatus, 500);


    $scope.$on('hub:assign', function () {
      getProductionStatus();
    });

    $scope.$on('hub:unassign', function () {
      getProductionStatus();
    });

    $scope.$on('hub:inject', function () {
      debounced();
    });

    $scope.$on('hub:started', function () {
      debounced();
    });

    $scope.$on('hub:ended', function () {
      debounced();
    });

    $scope.$on('productionstatus:created', function (event, data) {
      if (data.data.hub === $rootScope.hub.id) {
        debounced();
      }
    });

    $scope.$on('productionstatus:updated', function (event, data) {
      if (data.data.hub === $rootScope.hub.id) {
        debounced();
      }
    });

    $scope.$watch(angular.bind(vm, function () {
      return vm.selected;
    }), function (newValue, oldValue) {
      if (newValue) {
        vm.selectedResource = newValue.resource.id;
        vm.selectedOperation = newValue.operation;
        if (!$rootScope.hub.id) {
          return true;
        }
        if (vm.selectedResource !== $rootScope.hub.targetResource || vm.selectedOperation !== $rootScope.hub.operation) {
          hubService.working({
            resource: vm.selectedResource,
            operation: vm.selectedOperation
          }).$promise.then(function (success) {
            $rootScope.$broadcast('changeWorking');
          });
          debounced();
        }
      }
    });

    function change(l) {
      vm.selected = l;
    }

  }
})();