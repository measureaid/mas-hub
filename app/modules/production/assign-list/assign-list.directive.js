(function () {
  'use strict';
  /* globals _ */
  angular
    .module('masApp.production')
    .directive('productionAssignList', productionAssignList);

  /* @ngInject */
  function productionAssignList() {
    var directive = {
      restrict: 'EA',
      templateUrl: 'modules/production/assign-list/assign-list.tmpl.html',
      scope: {
        selected: '=?selectedProduction',
        productionAssigns: '=?assigned',
        maxQuantity: '=?'
        // selectedResource: '=?selectedResource',
        // selectedOperation: '=?selectedOperation'
      },
      controller: Controller,
      controllerAs: 'vm',
      bindToController: true
    };

    return directive;
  }


  /* @ngInject */
  function Controller(ProductionAssign, $rootScope, $scope, hubService, ProductionStatus, Bundle) {
    var vm = this;

    vm.change = change;
    vm.containerCapacityOrPackageQuantity = 0;

    var productionStatus = [];


    vm.unassign = function (l) {
      return hubService.unassign({
        production: l.productionIds
      });
    };

    vm.selectedCss = function (item) {
      if (item.resource === $rootScope.hub.targetResource) {
        return 'hover';
      } else {
        return null;
      }
    };


    $rootScope.$watch('hub.id', function (newValue, oldValue) {
      if (newValue) {

        getContainerCapacityOrPackageUnit();
        getProductionStatus();

      }
    }, true);

    $scope.$on('equipment:updated', function (event, data) {
      getProductionStatus();
    });
    $scope.$on('bundle:updated', function (event, data) {
      var work_container = $rootScope.hub.machine ? $rootScope.hub.machine.work_container : $rootScope.hub.work_container;
      if (work_container === data.data.container) {
        getProductionStatus();
      }
    });

    $scope.$on('productionassign:destroyed', function (event, data) {
      if (data.data.hub === $rootScope.hub.id) {
        getProductionStatus();
      }
    });
    $scope.$on('productionstatus:created', function (event, data) {
      if (data.data.hub === $rootScope.hub.id) {
        getProductionStatus();
      }
    });
    $scope.$on('productionstatus:updated', function (event, data) {
      var foundStatus = _.find(productionStatus, function (ps) {
        return ps.id === data.id;
      });
      if (foundStatus) {
        getProductionStatus();
      }
    });

    $scope.$watch(angular.bind(vm, function () {
      return vm.selected;
    }), function (newValue, oldValue) {
      if (newValue) {
        vm.selectedResource = newValue.resource;
        vm.selectedOperation = newValue.operation;
        if (!$rootScope.hub.id) {
          return true;
        }
        if (vm.selectedResource !== $rootScope.hub.targetResource || vm.selectedOperation !== $rootScope.hub.operation) {
          hubService.working({
            resource: vm.selectedResource,
            operation: vm.selectedOperation
          }).$promise.then(function (success) {
            $rootScope.$broadcast('changeWorking');
          });
          getContainerCapacityOrPackageUnit();
        }
      }
    });

    // function merge(data) {
    //   var foundData = _.find(vm.list, function (condition) {
    //     var resourceIds = _.compact(_.uniq([condition.resource_data.id, condition.resource_data.target, condition.resource_data.wip]));

    //     return _.includes(resourceIds, data.data.resource);
    //   });

    //   if (foundData) {
    //     var stat = {
    //       work_quantity: getSumByWorkQuatity(foundData.resource_data),
    //       piled_quantity: getSumByPiledQuatity(foundData.resource_data),
    //       remainder_quantity: getSumByRemainderQuantity(foundData.resource_data),
    //       defect_quantity: getSumByDefectQuantity(foundData.resource_data)
    //     }
    //     _.merge(foundData.stat, stat);
    //     // getContainerCapacityOrPackageUnit();
    //     $scope.$apply();
    //   }
    // }

    function calc() {
      if (!$rootScope.hub.id) {
        return null;
      }
      // console.log($rootScope.hub);
      var grouppedProductions = _.groupBy(vm.productions, 'resource');
      vm.list = _.map(grouppedProductions, function (value, key) {
        var resource_data = _.first(value).resource_data;
        // console.log(resource_data);
        return {
          resource: _.first(_.uniq(_.map(value, 'resource'))),
          resource_data: resource_data,
          operation: _.first(_.uniq(_.map(value, 'operation'))),
          operation_data: _.first(value).operation_data,
          productionIds: _.uniq(_.map(value, 'parent')),
          stat: {
            work_quantity: getSumByWorkQuatity(resource_data),
            piled_quantity: getSumByPiledQuatity(resource_data),
            remainder_quantity: getSumByRemainderQuantity(resource_data),
            defect_quantity: getSumByDefectQuantity(resource_data)
          }
        };
      });
    }

    function getContainerCapacityOrPackageUnit() {
      if ($rootScope.hub.id) {
        if (_.get($rootScope.hub.operation_data, 'config.package') && !_.isNull(vm.productions) && !_.isEmpty(vm.productions) && !_.isUndefined(vm.productions)) {
          hubService.packageUnit().$promise.then(function (packageUnit) {
            vm.containerCapacityOrPackage = '포장 단위 수량 (' + packageUnit.bundle_unit + ')';
            vm.containerCapacityOrPackageQuantity = packageUnit.quantity;
          });
        } else if (!_.isNull(vm.productions) && !_.isEmpty(vm.productions) && !_.isUndefined(vm.productions)) {
          hubService.containerCapacity().$promise.then(function (containerCapacity) {
            vm.containerCapacityOrPackage = '공정용 박스 단위 수량 (' + $rootScope.hub.target_resource_data.unit + ')';
            vm.containerCapacityOrPackageQuantity = containerCapacity.quantity;
          });
        }
      }
    }

    function getProductionStatus() {
      if ($rootScope.hub.id) {
        var bundle_query = {
          where: {
            bundle_unit: null
          },
          populate: 'production'
        };

        if (_.get($rootScope.hub, 'operation_data.mold')) {
          bundle_query.where.resource = $rootScope.hub.target_resource_data.wip;
        }
        bundle_query.where.container = $rootScope.hub.machine ? [$rootScope.hub.machine.finish_container, $rootScope.hub.machine.work_container] : [$rootScope.hub.finish_container, $rootScope.hub.work_container];

        Bundle.list(bundle_query).$promise.then(function (bundles) {
          ProductionAssign.list({
            equipment: $rootScope.hub.id,
            populate: 'production'
          }).$promise.then(function (success) {
            var query = {
              where: {
                hub: $rootScope.hub.id,
              }
            };
            vm.bundles = _.groupBy(bundles.result, 'resource');
            var productionAssign_Productions = _.map(success.result, 'production');
            var container_Productions = _.map(bundles.result, 'production');
            vm.productions = _.concat(productionAssign_Productions, container_Productions);
            query.where.parentProduction = _.concat(_.compact(_.uniq(_.map(vm.productions, 'parent'))));

            ProductionStatus.list(query).$promise.then(function (result) {
              productionStatus = result.result;
              calc();
              getContainerCapacityOrPackageUnit();
            });
          });
        });
      }
    }

    function getSumByWorkQuatity(resource) {
      var productionIds = _.compact(_.uniq(_.map(vm.productions, 'parent')));
      var resourceIds = _.compact(_.uniq([resource.id, resource.target, resource.wip]));
      var filteredProductionStatus = _.filter(productionStatus, function (item) {
        if (_.includes(resourceIds, item.resource) && _.includes(productionIds, item.parentProduction)) {
          return true;
        }
      });
      var ret = null;
      if (!_.isNull(filteredProductionStatus) && !_.isEmpty(filteredProductionStatus) && !_.isUndefined(filteredProductionStatus)) {
        var end_quantity = _.sumBy(filteredProductionStatus, 'end_quantity');
        var start_quantity = _.sumBy(filteredProductionStatus, 'start_quantity');
        ret = start_quantity - end_quantity;
      }
      return ret;
    }

    function getSumByRemainderQuantity(resource) {
      var ret = null;

      var productionIds = _.compact(_.uniq(_.map(vm.productions, 'parent')));
      var resourceIds = _.compact(_.uniq([resource.id, resource.target, resource.wip]));
      var filteredProductionStatus = _.filter(productionStatus, function (item) {
        if (_.includes(resourceIds, item.resource) && _.includes(productionIds, item.parentProduction)) {
          return true;
        }
      });
      var temp = _.sortBy(_.compact(_.uniq(_.map(filteredProductionStatus, 'parentProduction'))));
      if (temp.length == 1) {
        ret = 0;
      }
      var pick_ps = _.filter(filteredProductionStatus, function (each) {
        return each.parentProduction === _.first(temp);
      });
      if (!_.isNull(pick_ps) && !_.isEmpty(pick_ps) && !_.isUndefined(pick_ps)) {
        var end_quantity = _.sumBy(pick_ps, 'end_quantity');
        var collect_quantity = _.sumBy(pick_ps, 'collect_quantity');
        var package_quantity = _.sumBy(pick_ps, 'package_quantity');
        var defect_quantity = _.sumBy(pick_ps, 'defect_quantity');
        ret = end_quantity - collect_quantity - package_quantity - defect_quantity;
      }
      return ret;
    }

    function getSumByPiledQuatity(resource) {
      var productionIds = _.compact(_.uniq(_.map(vm.productions, 'parent')));
      var resourceIds = _.compact(_.uniq([resource.id, resource.target, resource.wip]));
      var filteredProductionStatus = _.filter(productionStatus, function (item) {
        if (_.includes(resourceIds, item.resource) && _.includes(productionIds, item.parentProduction)) {
          return true;
        }
      });
      var quantity = null;
      if (!_.isNull(filteredProductionStatus) && !_.isEmpty(filteredProductionStatus) && !_.isUndefined(filteredProductionStatus)) {
        var end_quantity = _.sumBy(filteredProductionStatus, 'end_quantity');
        var collect_quantity = _.sumBy(filteredProductionStatus, 'collect_quantity');
        var package_quantity = _.sumBy(filteredProductionStatus, 'package_quantity');
        var defect_quantity = _.sumBy(filteredProductionStatus, 'defect_quantity');
        quantity = end_quantity - collect_quantity - package_quantity - defect_quantity;
        vm.maxQuantity = quantity;
      }
      return quantity;
    }

    function getSumByDefectQuantity(resource) {
      var productionIds = _.compact(_.uniq(_.map(vm.productions, 'parent')));
      var resourceIds = _.compact(_.uniq([resource.id, resource.target, resource.wip]));
      var filteredProductionStatus = _.filter(productionStatus, function (item) {
        if (_.includes(resourceIds, item.resource) && _.includes(productionIds, item.parentProduction)) {
          return true;
        }
      });
      var quantity = null;

      if (!_.isNull(filteredProductionStatus) && !_.isEmpty(filteredProductionStatus) && !_.isUndefined(filteredProductionStatus)) {
        quantity = _.sumBy(filteredProductionStatus, 'defect_quantity');
      }
      return quantity;
    }

    function change(l) {
      vm.selected = l;
    }


  }
})();