(function() {
  'use strict';
  /* globals _ */
  angular
    .module('masApp.production')
    .directive('simpleProduction', simpleProduction);

  /* @ngInject */
  function simpleProduction() {
    var directive = {
      restrict: 'EA',
      templateUrl: 'modules/production/simple-production/simple-production.tmpl.html',
      scope: {
        selected: '=?selectedProduction',
        productionstatuss: '=?statused'
      },
      controller: Controller,
      controllerAs: 'vm',
      bindToController: true
    };

    return directive;
  }


  /* @ngInject */
  function Controller(Production, ProductionAssign, ProductionStatus, $rootScope, $scope, hubService, MAS_C, Bundle, MASConfig) {
    var vm = this;

    vm.change = change;
    vm.class = [];

    vm.unassign = function(l) {
      return hubService.unassign({
        production: l.productionIds
      });
    };

    vm.selectedCss = function(item) {
      if (item.id === _.get(vm.assign, 'production.id')) {
        return 'hover';
      } else {
        return null;
      }
    };


    $rootScope.$watch('hub.id', function(newValue) {
      if (newValue) {
        vm.getProduction();
      }
    });

    vm.getProduction = function() {
      if ($rootScope.hub) {
        if (_.get($rootScope.hub, 'machine')) {
          ProductionAssign.list({
            equipment: $rootScope.hub.id,
            populate: 'production'
          }).$promise.then(function(assign) {
            vm.assign = _.first(assign.result);
            $rootScope.production = _.get(vm.assign, 'production');

            ProductionStatus.read({
              where: {
                hub: $rootScope.hub.id,
                production: vm.assign.production.id
              }
            }).$promise.then(function(status) {
              vm.status = _.first(status.result);
            });
            Production.read({
              id: $rootScope.production.id,
              populate: ['parent']
            }).$promise.then(function(production) {
              vm.production = production;
            });
          });
        } else if (!_.get($rootScope.hub, 'machine')) {
          Production.list({
            sort: 'id',
            $unlimit: true,
            where: {
              operation: $rootScope.hub.operation,
              status: {
                '!': MAS_C.PRODUCTION.STATUS.COMPLETE
              }
            },
            populate: ['parent']
          }).$promise.then(function(productions) {
            if (!vm.productionList) {
              vm.productionList = productions.result;
            } else {
              _.merge(vm.productionList, productions.result);
            }
          });
          ProductionAssign.list({
            equipment: $rootScope.hub.id,
            populate: 'production'
          }).$promise.then(function(assign) {
            vm.assign = _.first(assign.result);
            $rootScope.production = _.get(vm.assign, 'production');

            if (_.get(vm.assign, 'production')) {
              getPackable();
              ProductionStatus.read({
                where: {
                  production: vm.assign.production.id
                }
              }).$promise.then(function(status) {
                vm.status = {
                  end_quantity: _.sumBy(status.result, 'end_quantity'),
                  defect_quantity: _.sumBy(status.result, 'defect_quantity')
                }
                // vm.status = _.first(status.result);
              });
            }
          });
        }
      }
    }


    var debounced = _.debounce(vm.getProduction, 500);


    $scope.$on('production:created', function() {
      debounced();
    });

    $scope.$on('production:updated', function(event, data) {
      if (data.data.status === MAS_C.PRODUCTION.STATUS.COMPLETE) {
        _.remove(vm.productionList, function(list) {
          return list.id = data.id
        });
      }
    });

    $scope.$on('bundle:updated', function() {
      debounced();
    });


    $scope.$on('hub:assign', function() {
      debounced();
    });

    $scope.$on('hub:unassign', function() {
      debounced();
    });

    $scope.$on('hub:inject', function() {
      debounced();
    });

    $scope.$on('hub:started', function() {
      debounced();
    });

    $scope.$on('hub:ended', function() {
      debounced();
    });

    $scope.$on('productionstatus:created', function(event, data) {
      if (data.data.hub === $rootScope.hub.id) {
        debounced();
      }
    });

    $scope.$on('productionstatus:updated', function(event, data) {
      if (data.data.hub === $rootScope.hub.id) {
        debounced();
      }
    });

    $scope.$watch(angular.bind(vm, function() {
      return vm.selected;
    }), function(newValue, oldValue) {

      if (newValue) {
        hubService.assign({
          production: newValue.id,
          equipment: $rootScope.hub.id
        }).$promise.then(function(success) {
          console.log(success);
        });
        vm.selectedResource = newValue.resource.id;
        vm.selectedOperation = newValue.operation;
        if (!$rootScope.hub.id) {
          return true;
        }
        if (vm.selectedResource !== $rootScope.hub.targetResource || vm.selectedOperation !== $rootScope.hub.operation) {
          hubService.working({
            resource: vm.selectedResource,
            operation: vm.selectedOperation
          }).$promise.then(function(success) {
            $rootScope.$broadcast('changeWorking');
          });
          debounced();
        }
      }
    });

    function getPackable() {
      if (_.get(vm.assign, 'production')) {
        Bundle.list({
          where: {
            container: MASConfig.configs.floatContainer,
            parentProduction: vm.assign.production.parent,
            resource: vm.assign.production.resource_data.wip
          }
        }).$promise.then(function(bundles) {
          var bundles = bundles.result;
          vm.packableQuantity = _.sumBy(bundles, 'quantity');
        });
      }
    }

    function change(l) {
      $rootScope.production = l;
      vm.selected = l;
    }


  }
})();