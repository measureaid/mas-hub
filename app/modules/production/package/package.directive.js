(function() {
  'use strict';

  angular
    .module('masApp.production')
    .directive('packageProduct', packageProduct);

  /* @ngInject */
  function packageProduct() {
    var directive = {
      restrict: 'EA',
      templateUrl: 'modules/production/package/package.tmpl.html',
      scope: {
        production: '=?',
        resource: '=?',
        packageShow: '=?'
      },
      controller: Controller,
      controllerAs: 'vm',
      bindToController: true
    };
    return directive;
  }

  /* @ngInject */
  function Controller(hubService, $rootScope, UnitFactory, toastr, $scope) {
    var vm = this;
    vm.remainderChange = remainderChange;
    vm.packageProduct = packageProduct;
    vm.remainder = false;

    $scope.$watch(angular.bind(vm, function() {
      return vm.packageShow;
    }), function(newValue) {
      if (!newValue) {
        vm.remainderNum = null;
        vm.remainder = false;
      }
    });

    ///////////////////////

    function remainderChange() {
      if (vm.remainder) {
        vm.remainder = false;
      } else {
        if ($rootScope.remainderQuantity && $rootScope.remainderQuantity !== 0) {
          vm.remainderNum = $rootScope.remainderQuantity;
        }
        vm.remainder = true;
      }
    }

    function packageProduct() {
      vm.packageShow = false;
      return hubService.package({
        // resourceId: vm.resource,
        // productionId: vm.production,
        quantity: vm.remainderNum ? vm.remainderNum : vm.packageQuantity
      }).$promise.then(function(result) {
        vm.remainderNum = null;
        vm.remainder = false;
        $rootScope.remainderQuantity = 0;
      }, function(err) {
        toastr.error(err.body.message, '포장 오류');
      });
    }



  }
})();