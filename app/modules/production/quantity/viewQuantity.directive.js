(function () {
	'use strict';

	angular
		.module('masApp.production')
		.directive('viewQuantity', viewQuantity);

	/* @ngInject */
	function viewQuantity() {
		var directive = {
			restrict: 'EA',
			templateUrl: 'modules/production/quantity/view-quantity.tmpl.html',
			scope: {
				productionId: '=',
				production: '=?'
			},
			controller: viewQuantityController,
			controllerAs: 'vm',
			bindToController: true
		};

		return directive;

	}

	/* @ngInject */
	function viewQuantityController($scope, Production) {
		var vm = this;
		// $scope.$watch(angular.bind(vm, function () {
		// 	return vm.production;
		// }), function (newValue) {
		// 	if (newValue) {
		// 		console.log(newValue);
		// 	}
		// });
		// $scope.$on('production:reload', function () {
		// 	activate();
		// });
		//
		//
		// activate();
		//
		// function activate() {
		// 	if ($scope.productionId) {
		// 		Production.read({
		// 			id: $scope.productionId,
		// 			populate: ['parent']
		// 		}).$promise.then(function (success) {
		// 			$scope.production = success;
		// 			$scope.production.ready_complete_quantity = Math.floor($scope.production.ready_complete_quantity);
		// 		});
		// 	} else {
		// 		$scope.production = {};
		// 	}
		// }
	}
})();
