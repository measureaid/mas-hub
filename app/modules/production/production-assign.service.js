(function() {
  'use strict';

  angular
    .module('masApp.production')
    .service('ProductionAssign', ProductionAssign);

  function ProductionAssign(masResource) {

    var S = masResource('productionassign', {
      list: {
        method: 'GET',
      }
    });
    return S;
  }
})();