(function() {
    'use strict';

    angular
        .module('masApp.inventory')
        .service('Inventory', Inventory);

    Inventory.$inject = ['masResource'];

    function Inventory(masResource) {
        var S = masResource('inventory', {
            // Inventory
            list: {
                method: 'GET',
            },
            enter: {
                method: 'POST',
            },
            read: {
                method: 'GET',
            },
            consume: {
                method: 'POST',
                url: '/inventory/:id/consume'
            },
            move: {
                method: 'POST',
                url: '/inventory/:id/move'
            },
            loss: {
                method: 'POST',
                url: '/inventory/:id/loss'
            },
            junk: {
                method: 'POST',
                url: '/inventory/:id/junk'
            },
            deliver: {
                method: 'POST',
                url: '/inventory/:id/deliver'
            },
            recommend: {
                method: 'POST',
                url: '/inventory/:id/recommend'
            },
            printForm: {
                method: 'GET',
                isArray: true,
                url: '/inventory/:id/printForm'
            },
            log: {
                method: 'GET',
                url: '/inventory/log'
            },
        });
        return S;
    }
})();