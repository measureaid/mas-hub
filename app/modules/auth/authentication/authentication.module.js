(function() {
  'use strict';

  angular
    .module('mas.user.authentication', [
      'LocalStorageModule'
    ]);
})();