/*jslint bitwise: true */
/* globals _, pushNotification, io*/
(function() {
  'use strict';

  angular.module('mas.user.authentication')

    .provider('Auth',
      function() {
        var vm = this;

        return {
          $get: function($injector) {

            var rScope = $injector.get('$rootScope');
            var $q = $injector.get('$q');
            var $window = $injector.get('$window');

            var masResource = $injector.get('masResource');
            var AuthPersistance = $injector.get('AuthPersistance');
            var USER_ROLES = $injector.get('USER_ROLES');
            var MASFactory = $injector.get('MASFactory');
            var AUTH_SERVER_CONFIG = $injector.get('AUTH_SERVER_CONFIG');
            var AuthServer = masResource('auth', AUTH_SERVER_CONFIG);
            var ACCESS_LEVELS = $injector.get('ACCESS_LEVELS');

            vm.dbUser = AuthPersistance.user();
            rScope.currentUser = vm.dbUser;

            var initCurrentUser = function() {
              if (_.isUndefined(vm.dbUser) || _.isEmpty(vm.dbUser)) {
                return initUser();
              } else {
                rScope.currentUser = vm.dbUser;
                if (!_.get(vm, 'dbUser.role.bitMask') || !_.get(vm, 'dbUser.role.title') || !_.isEqual(parseInt(vm.dbUser.role.bitMask), parseInt(USER_ROLES[vm.dbUser.role.title].bitMask))) {
                  return initUser();
                }
              }
            };
            initCurrentUser();

            function initUser() {
              var emptyUser = {
                role: USER_ROLES.public
              };
              AuthPersistance.clearAll();
              return changeUser(emptyUser);
            }

            function changeUser(user) {
              if (!_.isEqual(user.role, _.get(rScope, 'currentUser.role'))) {
                // angular.extend(rScope.currentUser, user);
                var filteredUser = MASFactory.blacklist(user, ['passports', 'profiles', 'mask', 'identifier', 'password', '$promise', '$resolved']);
                rScope.currentUser = filteredUser;
                AuthPersistance.changeUser(filteredUser);
                console.log('유저 변경 시도 : ', rScope.currentUser);
                rScope.$broadcast('USER_CHANGED', rScope.currentUser);
              }
            }

            return {
              permissionChanged: function(user) {
                return changeUser(user);
              },

              updateUser: function(user) {
                var userData = user;
                return changeUser(userData);
              },
              authorize: function(al, role) {
                var accessLevel = ACCESS_LEVELS[al || 'public'];

                if (_.isUndefined(role)) {
                  role = rScope.currentUser.role;
                }
                if (_.isUndefined(accessLevel)) {
                  return;
                }
                return parseInt(accessLevel.bitMask) & parseInt(role.bitMask);
              },

              isLoggedIn: function(user) {
                //  console.info('현재 유저', user, currentUser);
                if (_.isUndefined(user)) {
                  if (_.isUndefined(rScope.currentUser)) {
                    initCurrentUser();
                  }
                  user = rScope.currentUser;
                }
                return user.role.title !== USER_ROLES.public.title;
              },

              //TODO: findpassword
              findpassword: function(email, success, error) {
                return AuthServer.findpassword({
                  email: email
                }, function(res) {
                  success(res);
                }, function(err) {
                  error(err);
                });
              },
              //TODO: resetpassword
              resetpassword: function(password, success, error) {
                return AuthServer.resetpassword({
                  password: password
                }, function(res) {
                  success(res);
                }, function(err) {
                  error(err);
                });
              },

              register: function(form, success, error) {
                return AuthServer.register(form, function(res) {
                  success(res);
                }, function(err) {
                  error(err);
                });
              },

              loginByBarcode: function(token, success, error) {
                return AuthServer.loginByBarcode(token)
                  .$promise
                  .then(function(res) {
                    console.log('로그인 성공', res);

                    var resUserTemp = JSON.parse(angular.toJson(res));
                    var resUser = {
                      role: {
                        title: resUserTemp.role,
                        bitMask: resUserTemp.mask
                      },
                      // partner: resUserTemp.partner
                    };

                    delete resUserTemp.mask;
                    delete resUserTemp.role;
                    angular.extend(resUser, resUserTemp);
                    changeUser(resUser);
                    if (success && typeof(success) === 'function') {
                      success(resUser);
                    }
                  }).catch(function(err) {
                    AuthPersistance.clearAll();
                    initUser();
                    if (error && typeof(error) === 'function') {
                      error(err);
                    }
                  });
              },

              login: function(user, success, error) {
                return AuthServer.login(user)
                  .$promise
                  .then(function(res) {
                    var resUserTemp = angular.fromJson(res);
                    console.log(resUserTemp);

                    var resUser = {
                      role: USER_ROLES[resUserTemp.role]
                    };
                    delete resUserTemp.mask;
                    delete resUserTemp.role;
                    angular.extend(resUser, resUserTemp);
                    changeUser(resUser);
                    if (success && _.isFunction(success)) {
                      success(resUser);
                    }
                  })
                  .catch(function(err) {
                    initUser();
                    if (error && _.isFunction(error)) {
                      error(err);
                    }
                  });
              },
              logoutLocal: function(callback) {
                if (typeof pushNotification === 'object') {
                  pushNotification.unregister();
                }
                initUser();
                callback();
              },
              logout: function(success) {
                return AuthServer.logout({
                  token: $window.fcm_token
                }).$promise.
                then(function() {
                  if (typeof pushNotification === 'object') {
                    pushNotification.unregister();
                  }
                  initUser();
                  if (success) {
                    success(rScope.currentUser);
                  }
                }, function() {
                  initUser();
                });
              }
            };
          }
        };
      });

})();