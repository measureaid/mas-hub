(function() {
  'use strict';
  angular.module('mas.user.authorization')

    .directive('accessLevel', function($rootScope, Auth, ACCESS_LEVELS, USER_ROLES, $animate) {
      return {
        restrict: 'A',
        multiElement: true,
        transclude: 'element',
        priority: 590,
        terminal: true,
        scope: {
          accessLevel: '@'
        },
        $$tlb: true,

        link: function($scope, $element, $attr, ctrl, $transclude) {
          var block, childScope, previousElements,
            getBlockNodes;
          var userRole;

          getBlockNodes = function(nodes) {
            var node = nodes[0];
            var endNode = nodes[nodes.length - 1];
            var blockNodes = [node];

            do {
              node = node.nextSibling;
              if (!node) break;
              blockNodes.push(node);
            } while (node !== endNode);

            return angular.element(blockNodes);
          };

          if ($rootScope.currentUser && $rootScope.currentUser.hasOwnProperty('role')) {
            userRole = $rootScope.currentUser.role;
          }
          var userRoleListener = $rootScope.$on('USER_CHANGED', function(event, data) {
            // console.info('유저 변경됨2', data.role);
            if (data.role) {
              userRole = data.role;
            }
            process();
          });

          $rootScope.$on('$destroy', function() {
            userRoleListener();
          });

          function process() {
            var al = $scope.accessLevel;

            var value = Auth.authorize(al) !== 0;
            if (value) {
              if (!childScope) {
                $transclude(function(clone, newScope) {
                  childScope = newScope;
                  clone[clone.length++] = document.createComment(' end hasPermission: ' + al + ' ');
                  block = {
                    clone: clone
                  };
                  $animate.enter(clone, $element.parent(), $element);
                });
              }
            } else {
              if (previousElements) {
                previousElements.remove();
                previousElements = null;
              }
              if (childScope) {
                childScope.$destroy();
                childScope = null;
              }
              if (block) {
                previousElements = getBlockNodes(block.clone);
                $animate.leave(previousElements).done(function(response) {
                  if (response !== false) previousElements = null;
                });
                block = null;
              }
            }

          }
          process();
        }
      };
    });

})();