(function() {
  'use strict';

  angular
    .module('mas.user', [
      'mas.user.authentication',
      'mas.user.authorization'
    ]);
})();