(function() {
    'use strict';
    /* global _ */
    angular
        .module('mas.unit')
        .service('Unit', Unit);

    function Unit(masResource) {
        var S = masResource('unit', {
            list: {
                method: 'GET',
            },
            create: {
                method: 'POST',
            },
            read: {
                method: 'GET',
                cache: true,
            },
            delete: {
                method: 'DELETE',
                isArray: true,
            },
        });

        return S;
    }
})();