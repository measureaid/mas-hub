(function() {
  'use strict';
  /* globals UnitNative, _ */
  angular
    .module('mas.unit')
    .factory('UnitFactory', unitFactory);

  var MathUnit = new UnitNative();

  function unitFactory(Unit) {

    var unit = {
      calc: conversion
    };
    unit.addUnit = addUnit;
    unit.updateUnit = updateUnit;
    unit.removeUnit = removeUnit;
    unit.UNITS = UNITS;
    unit.PREFIXES = PREFIXES;
    unit.BASE_UNITS = BASE_UNITS;
    unit.UNIT_SYSTEMS = UNIT_SYSTEMS;

    function conversion(value, symbol, targetSymbol) {
      try {
        if (!_.isNumber(value)) {
          throw new Error('conversion.bySymbol need value to convert');
        }
        if (_.isObject(symbol) && symbol.hasOwnProperty('symbol') && symbol.symbol) {
          symbol = symbol.symbol;
        }
        if (_.isObject(targetSymbol) && symbol.hasOwnProperty('symbol') && targetSymbol.symbol) {
          targetSymbol = targetSymbol.symbol;
        }
        if (!symbol || !targetSymbol || !_.isNumber(value)) {
          return;
        }

        var originalUnit = new MathUnit(value, symbol);
        var converted = originalUnit.toNumber(targetSymbol);
        return converted;
      } catch (e) {
        if (e.message !== 'Units do not match') {
          Unit.list({
            symbol: _.compact([targetSymbol, symbol])
          }).$promise.then(function(success) {
            // console.info(success.result);
            if (targetSymbol) {
              var target_Unit = _.find(success.result, {
                symbol: targetSymbol
              });
              // console.debug(target_Unit);
              addUnit(target_Unit);
            }
            if (symbol) {
              var original_Unit = _.find(success.result, {
                symbol: symbol
              });
              // console.debug(original_Unit);
              addUnit(original_Unit);
            }
            // console.log(MathUnit.UNITS);
            var originalUnit = new MathUnit(value, symbol);
            var converted = originalUnit.toNumber(targetSymbol);
            return converted;

          });
        }
        // , function () {
        // 	throw e;
        // }
      }
    }

    //////
    function addUnit(_unitSet) {
      try {
        if (!_unitSet.resource) {
          _unitSet.resource = null;
        }
        var ret;
        if (!MathUnit.UNITS[_unitSet.symbol]) {
          ret = MathUnit.addUnit(_unitSet.symbol, _unitSet.name, _unitSet.base, _unitSet.value);
        } else {
          ret = MathUnit.UNITS[_unitSet.symbol];
        }
        // table[_unitSet.symbol] = _unitSet;
        return ret;
      } catch (e) {
        throw e;
      }
    }

    function updateUnit(_unitSet) {
      try {
        conversion.removeUnit(_unitSet.symbol);
        return conversion.addUnit(_unitSet);
      } catch (e) {
        return e;
      }
    }

    function removeUnit(symbol) {
      try {
        var ret = MathUnit.removeUnit(symbol);
        return ret;
      } catch (e) {
        throw e;
      }
    }

    function UNITS() {
      return MathUnit.UNITS;
    }

    function PREFIXES() {
      return MathUnit.PREFIXES;
    }

    function BASE_UNITS() {
      return MathUnit.BASE_UNITS;
    }

    function UNIT_SYSTEMS() {
      return MathUnit.UNIT_SYSTEMS;
    }

    return unit;
  }


})();