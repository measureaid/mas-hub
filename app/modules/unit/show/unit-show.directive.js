(function () {
  'use strict';
  /* globals _, Unit */
  angular
    .module('mas.unit')
    .directive('unitShow', unitShow);

  /* @ngInject */
  function unitShow() {
    var directive = {
      restrict: 'EA',
      template: '<span md-colors="{color:\'primary\'}">{{vm.unit.name}}</span>',
      scope: {
        input: '=unitShow',
        quantity: '=?'
      },
      controller: Controller,
      controllerAs: 'vm',
      bindToController: true
    };

    return directive;

  }

  function Controller($scope, Unit, UnitFactory, $element) {
    var vm = this;
    // vm.isVisible = $element.is(':visible');

    // $scope.$watch(angular.bind(vm, function () {
    //   return $element.is(':visible');
    // }), function (newValue) {
    //   vm.isVisible = $element.is(':visible');

    //   if (newValue) {
    //     // console.log('VISIBLE!!', vm.input, arguments);
    //     activate();
    //   }
    // });

    $scope.$watch(angular.bind(vm, function () {
      return vm.input;
    }), function (newValue, oldValue) {
      if (newValue === oldValue) {
        return;
      }
      activate();
    });

    function activate() {
      // if (vm.isVisible) {
      if (_.isObject(vm.input) && vm.input.hasOwnProperty('symbol') && vm.input.symbol) {
        vm.unit = _.clone(vm.input);
      } else {
        if (vm.input) {
          var foundUnit = UnitFactory.UNITS()[vm.input];
          if (foundUnit) {
            vm.unit = foundUnit;
          } else {
            Unit.read({
              id: vm.input
            }).$promise.then(function (unit) {
              vm.unit = unit;
            }, function () {
              vm.unit = { symbol: vm.input, name: vm.input };
            });
          }
        }
      }
      vm.quantity = vm.unit.value;
      // }
    }

  }
})();