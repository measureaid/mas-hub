(function () {
  'use strict';

  angular
    .module('masApp.sensor')
    .service('Sensor', Sensor);

  Sensor.$inject = ['masResource'];

  function Sensor(masResource) {
    // console.log('Sensor SERVICE LOADED')
    var S = masResource('sensor', {
      list: {
        method: 'GET',
      },
      read: {
        method: 'GET',
      },
    });
    return S;
  }
})();