(function () {
	'use strict';

	angular
		.module('masApp.hub')
		.directive('spinNumberDigit', spinNumberDigit);

	/* @ngInject */
	function spinNumberDigit() {
		var directive = {
			restrict: 'EA',
			templateUrl: 'modules/common/spin-number/spin-number-digit.tmpl.html',
			scope: {
				number: '='
			},
			controller: Controller,
			controllerAs: 'vm',
			bindToController: true
		};

		return directive;
	}

	/* @ngInject */
	function Controller($scope) {
		var vm = this;
		$scope.upper = function () {
			if (vm.number === 9) {
				vm.number = 0;
			} else {
				vm.number++;
			}
		};

		$scope.lower = function () {
			if (vm.number === 0) {
				vm.number = 9;
			} else {
				vm.number--;
			}
		};

		activate();

		function activate() {
			if (!vm.number) {
				vm.number = 0;
			}
		}
	}
})();
