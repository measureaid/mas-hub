(function() {
  'use strict';

  angular
    .module('masApp.hub')
    .directive('spinNumber', spinNumber);

  /* @ngInject */
  function spinNumber() {
    var directive = {
      restrict: 'EA',
      templateUrl: 'modules/common/spin-number/spin-number.tmpl.html',
      scope: {
        digit: '=?',
        number: '=ngModel'
      },
      controller: Controller,
      controllerAs: 'vm',
      bindToController: true
    };

    return directive;
  }

  function Controller($scope, $rootScope) {
    var vm = this;
    if (!vm.digit) {
      vm.digit = 1;
    }

    vm.digits = [];

    $scope.$watch(angular.bind(vm, function() {
      return vm.digit;
    }), function(newValue, oldValue) {
      _.times(vm.digit, function(time) {
        var parsedNum = 0;
        // if (vm.number !== 0) {
        //   parsedNum = (vm.number / (Math.pow(10, (vm.digit - time))));
        //   var ret = sudoFixed(parsedNum - parseInt(parsedNum), 3);
        //   vm.digits.push({
        //     number: _.floor(ret, 1) * 10
        //   });
        // } else {
        vm.digits.push({
          number: parsedNum
        });
        // }
      });
    });

    $scope.$watch(angular.bind(vm, function() {
      return vm.digits;
    }), function(newValue, oldValue) {
      var number = 0;
      //위에부터 게산되려나?
      _.map(vm.digits, function(eachNumber, index) {
        var weight = vm.digit - index - 1;
        number += eachNumber.number * Math.pow(10, weight);
      });
      vm.number = number;
    }, true);

    $scope.$watch(angular.bind(vm, function() {
      return vm.number;
    }), function(newValue, oldValue) {
      if (newValue === oldValue) {
        return;
      }
      if (typeof(newValue) !== 'undefined') {
        setting();
      }
    });


    function setting() {
      if (!vm.number) {
        vm.number = 0;
      }
      if (typeof(vm.number) === 'undefined') {
        vm.number = 0;
      }

      var paddedNum = _.padStart(vm.number.toString(), vm.digit, '0');
      _.map(paddedNum, function(digit, index) {
        // var weight = vm.digit - index - 1;

        var parsedNum = parseInt(digit);
        if (typeof(parsedNum) === 'undefined') {
          parsedNum = 0;
        }
        if (vm.digits[index] && vm.digits[index].number !== parsedNum) {
          vm.digits[index].number = parsedNum;
        }
      });
    }

    /* 소수점 이하 자리수를 고정한다. */

    function sudoFixed(_input, _fraction, _role) {
      var role = 'R';
      var ieee = 0.000001;
      if (role && (_role === 'R' || _role === 'F' || _role === 'C')) {
        role = _role;
      }
      if (_.isNull(_input) || _.isUndefined(_input) || _.isNaN(_input)) {
        _input = 0;
      }

      if (Math.abs(_input - (_.round(_input, _fraction))) < ieee) {
        _input = _.round(_input, _fraction);
      }
      //소수의 자리수만큼 곱한다.
      var fraction = (_fraction) ? parseInt(_fraction) : 2;
      var temp = parseFloat(_input) * Math.pow(10, fraction);
      //정수화시킨다
      var realified;
      switch (_role) {
        case 'R':
          realified = Math.round(temp);
          break;
        case 'F':
          realified = Math.floor(temp);
          break;
        case 'C':
          realified = Math.ceil(temp);
          break;
        default:
          realified = Math.round(temp);
      }
      return realified / Math.pow(10, fraction);
    };


  }
})();