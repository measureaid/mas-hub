(function () {
  'use strict';

  angular
    .module('masApp.hub')
    .directive('recommendViewer', recommendViewer);

  /* @ngInject */
  function recommendViewer() {
    var directive = {
      restrict: 'EA',
      templateUrl: 'modules/common/recommend-viewer/recommend-viewer.tmpl.html',
      scope: {
        recommend: '=recommendViewer',
        selectedInventory: '&?',
        forced: '&?'
      },
      controller: Controller,
      controllerAs: 'vm',
      bindToController: true
    };
    return directive;
  }

  function Controller() {
    var vm = this;
    vm.selectInventory = selectInventory;
    vm.force = force;

    function selectInventory(id) {
      if (vm.selectedInventory()) {
        vm.selectedInventory()(id);
      }
    }
    function force() {
      if (vm.forced()) {
        vm.forced()();
      }
    }

  }
})();
