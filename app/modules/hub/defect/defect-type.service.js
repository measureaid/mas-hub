/* globals $, Notification*/
(function() {
  'use strict';

  angular
    .module('masApp.hub')
    .service('DefectType', defectType);

  function defectType($rootScope, masResource) {
    var S = masResource('defectType', {
      list: {
        method: 'GET',
      }
    });

    return S;

  }
})();