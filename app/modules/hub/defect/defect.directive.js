(function() {
  'use strict';

  angular
    .module('masApp.hub')
    .directive('defectForm', defectForm);

  /* @ngInject */
  function defectForm() {
    var directive = {
      restrict: 'EA',
      templateUrl: 'modules/hub/defect/defect.tmpl.html',
      scope: {
        productionId: '=',
        max: '@?',
        min: '@?',
        defectShow: '='
      },
      controller: Controller,
      controllerAs: 'vm',
      bindToController: true
    };
    return directive;
  }

  function Controller($scope, toastr, $rootScope, hubService, DefectType, Auth, MASConfig) {
    var vm = this;
    vm.defect = defect;
    vm.defects = {};
    vm.numOfDefect = new Array();
    vm.number = 0;


    $scope.$watch(angular.bind(vm, function() {
      return Auth.isLoggedIn();
    }), function(newValue, oldValue) {
      if (newValue) {
        DefectType.list().$promise.then(function(success) {
          vm.defectTypes = success.result;
        });
      }
    }, true);

    $scope.$watch('number', function(newValue, oldValue) {
      $scope.invalid = false;
      if ($scope.max) {
        if ($scope.number > $scope.max) {
          $scope.invalid = true;
        }
      }
      if ($scope.min) {
        if ($scope.number < $scope.min) {
          $scope.invalid = true;
        }
      }
    });

    $scope.$watch(angular.bind(vm, function() {
      return vm.defectShow;
    }), function(newValue) {
      if (!newValue) {
        vm.number = 0;
      }
    });

    function defect(type) {
      if (vm.number === 0) {
        return null;
      } else if (vm.number > vm.max) {
        return null;
      }
      vm.defectShow = false;

      return hubService.defect({
        finish_container: (_.get($rootScope.hub, 'machine')) ? undefined : MASConfig.configs.floatContainer,
        quantity: vm.number,
        parentProduction: _.get($rootScope, 'production.parent.id', $rootScope.production.parent),
        production: $rootScope.production.id,
        resource: $rootScope.hub.target_resource_data.wip,
        defectType: type,
        simple: (_.get($rootScope.hub, 'machine')) ? false : true
      }).$promise.then(function(result) {
        vm.number = 0;
      }, function(err) {
        toastr.error(err.body.message, '불량 입력 에러');
      });
    }

  }
})();