/* globals _ */
(function () {
	'use strict';

	angular
		.module('masApp.hub')
		.directive('selectPackaging', selectPackagingDirective);

	function selectPackagingDirective(Unit) {
		var selectResource = {
			restrict: 'EA',
			templateUrl: 'modules/hub/packaging/select.html',
			scope: {
				disabled: '=?ngDisabled',
			},
			link: linkFunc,
			controller: selectResourceController,
			controllerAs: 'vm',
		};

		function linkFunc(scope, elm, attr, ctrl) {

			// var formController = elm.controller('form') || {
			// 	$addControl: angular.noop
			// };
			// formController.$addControl(ctrl);
			//
			// scope.$on('$destroy', function() {
			// 	formController.$removeControl(ctrl);
			// });
			//
			// ctrl.$dirty = false;
			// ctrl.$valid = true;
			// ctrl.$invalid = false;
			// ctrl.$setPristine();

			//내부적으로 선택
			scope.select = {};

			//외부에서 변경 && 내부값으로 변경
			// scope.$watch('selected', function(newValue, oldValue) {
			// 	//만약, 변경되었는데 object가 아니라 값이라면 외부에서 변경된것으로 생각.
			// 	if (!_.isObject(scope.selected)) {
			// 		if (newValue) {
			// 			Unit.read({
			// 				symbol: scope.selected
			// 			}, function(success) {
			// 				scope.select.selected = success;
			// 				scope.orig_select = _.clone(scope.selected);
			// 			});
			//
			// 		} else {
			// 			scope.select.selected = null;
			// 			scope.packaging = {};
			// 		}
			// 	}
			// });

			//내부적으로 변경
			scope.$watch('select.selected', function (newValue, oldValue) {
				if(newValue === oldValue) {
					return;
				}

				if(newValue) {
					if(newValue.hasOwnProperty('symbol')) {
						// console.log("리소스에 저장", newValue);
						scope.packaging = newValue;
						ctrl.$setViewValue(newValue.symbol);
						if(newValue.symbol !== scope.orig_select) {
							ctrl.$dirty = true;
						} else {
							ctrl.$dirty = false;
						}
					} else {
						// console.log("원래 리소스", scope.packaging);
						//
						scope.packaging = _.find(scope.units, {
							symbol: newValue
						});

						ctrl.$setViewValue(newValue);
						if(newValue !== scope.orig_select) {
							ctrl.$dirty = true;
						} else {
							ctrl.$dirty = false;
							ctrl.$setPristine();
						}
					}
				} else {
					scope.select.selected = null;
					scope.packaging = {};
				}
			}, true);
		}
		return selectResource;
	}

	selectResourceController.$inject = ['Unit', '$sce', '$scope', 'hubService', '$rootScope'];

	function selectResourceController(Unit, $sce, $scope, hubService, $rootScope) {
		var vm = this;

		$scope.trustAsHtml = function (value) {
			return $sce.trustAsHtml(value);
		};
		$scope.packaging = function () {
			hubService.packaging({}, {
				hub: $rootScope.hub.id,
			}).$promise.then(function () {
				setTimeout(function () {
					$rootScope.reloadState();
				}, 300);
			}, function () {
				$rootScope.reloadState();
			});
		}
		$scope.loadUnits = function (input) {
			var q = {};
			if(!$scope.option) {
				// q.componentable = true;
			}
			if(input) {
				q.query = input;
			}
			Unit.list(q, function (success) {
				$scope.units = success.result;
				// $scope.units.unshift({
				// 	id: null,
				// 	name: '선택하지 않음'
				// });
				// console.log($scope.units);
				// filterResource($scope.filter);
				// optionResource($scope.option);
			});
		};

		$scope.loadUnits();

	}
})();
