(function() {
  'use strict';
  /* globals _ */
  angular
    .module('masApp.hub')
    .directive('hubWorkContainer', hubWorkContainerView);

  /* @ngInject */
  function hubWorkContainerView() {
    var directive = {
      restrict: 'EA',
      templateUrl: 'modules/hub/work-container/hub-work-container.tmpl.html',
      scope: {},
      controller: HubWorkContainerController,
      controllerAs: 'vm',
      bindToController: true
    };
    return directive;
  }
  /* @ngInject */
  function HubWorkContainerController($rootScope, $scope, Bundle, FormFactory, Auth) {
    var vm = this;
    vm.materials = [];

    $rootScope.$watch('hub', function(newValue, oldValue) {
      console.log(newValue);
      console.log($rootScope.hub);
      vm.workContainerId = $rootScope.hub.machine ? $rootScope.hub.machine.work_container : $rootScope.hub.work_container;
      console.log(vm.workContainerId);
    }, true);

    var getData = function() {
      $scope.$watch(angular.bind(vm, function() {
        return vm.workContainerId;
      }), function(newValue, oldValue) {
        if (vm.workContainerId && Auth.isLoggedIn()) {
          getBundles();
          $scope.$on('inventory:created', function(event, data) {
            console.log('일로 오는지2', data);
            if (data.data.container === vm.workContainerId) {
              getBundles();
            }
          });
          $scope.$on('bundle:created', function(event, data) {
            console.log('일로 오는지3', data);
            if (data.data.container === vm.workContainerId) {
              var foundBundle = _.find(vm.bundles, {
                id: data.id
              });
              if (!foundBundle) {
                vm.bundles.push(data.data);
              } else {
                FormFactory.merge(data.data, vm.bundles);
              }
              grouping();
            }
          });
          // $scope.$on('bundle:destroyed', function(event, data) {
          //   console.log('일로 오는지4', data);
          //   _.remove(vm.bundles, {
          //     id: data.id
          //   });
          //   grouping();
          // });
          $scope.$on('bundle:destroyed', function(event, data) {
            console.log('일로 오는지4', data, vm.bundles);
            _.remove(vm.bundles, {
              id: data.id
            });
            grouping();
          });
          $scope.$on('bundle:updated', function(event, data) {
            console.log('일로 오는지5', data);
            var foundBundle = _.find(vm.bundles, {
              id: data.id
            });
            console.log(foundBundle);
            if (foundBundle) {
              if (data.data.container !== vm.workContainerId) {
                console.log('????????????????');
                _.remove(vm.bundles, {
                  id: data.id
                });
              } else {
                FormFactory.merge(data.data, vm.bundles);
              }
            } else if (data.data.container === vm.workContainerId) {
              vm.bundles.push(data.data);
            }
            grouping();
          });
        }
      });
    };

    if (Auth.isLoggedIn()) {
      console.log('일로 오는지3');
      getData();
    } else {
      console.log('일로 오는지7');
      var userWatch = $rootScope.$on('USER_CHANGED', function(event, data) {
        if (Auth.isLoggedIn()) {
          getData();
          userWatch();
        }
      });
    }


    $scope.sumQuantity = function(bundles) {
      return _.sumBy(bundles, 'quantity');
    };

    function grouping() {
      console.log(vm.bundles);
      vm.materials = _.groupBy(vm.bundles, 'stock');
      console.log(vm.materials);
    }

    function getBundles() {
      if (_.isUndefined(vm.workContainerId)) {
        vm.materials = [];
        return null;
      }
      return Bundle.list({
        sort: '-createdAt',
        where: {
          container: vm.workContainerId,
        },
        populate: ['unit', 'updator']
      }).$promise.then(function(result) {
        vm.bundles = result.result;
        grouping();
      });

    }
  }
})();
