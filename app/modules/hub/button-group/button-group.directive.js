(function() {
  'use strict';

  angular
    .module('masApp.hub')
    .directive('buttonGroup', buttonGroup);

  /* @ngInject */
  function buttonGroup(ProductionStatus) {
    var directive = {
      restrict: 'EA',
      templateUrl: 'modules/hub/button-group/button-group.tmpl.html',
      scope: {
        production: '=?',
        resource: '=?',
        assigned: '=?',
        endOnly: '&?',
        simpleCollect: '&?'
      },
      controller: Controller,
      controllerAs: 'vm',
      bindToController: true
    };

    return directive;
  }

  /* @ngInject */
  function Controller() {
    var vm = this;

    vm.packageShow = false;
    vm.defectShow = false;

    vm.showPackageButton = showPackageButton;
    vm.showDefectButton = showDefectButton;
    vm.funcEndOnly = funcEndOnly;
    vm.funcSimpleCollect = funcSimpleCollect;

    function funcEndOnly() {
      vm.endOnly()();
    }

    function funcSimpleCollect() {
      vm.simpleCollect()();
    }

    function showDefectButton() {
      if (vm.defectShow) {
        vm.defectShow = false;
      } else {
        vm.defectShow = true;
      }
    }

    function showPackageButton() {
      if (vm.packageShow) {
        vm.packageShow = false;
      } else {
        vm.packageShow = true;
      }
    }
  }

})();