(function() {
  'use strict';
  /* globals _ */
  angular
    .module('masApp.hub')
    .directive('hubFinishContainer', hubFinishContainer);

  /* @ngInject */
  function hubFinishContainer() {
    var directive = {
      restrict: 'EA',
      templateUrl: 'modules/hub/finish-container/hub-finish-container.tmpl.html',
      scope: {

      },
      controller: Controller,
      controllerAs: 'vm',
      bindToController: true
    };

    return directive;
  }

  /* @ngInject */
  function Controller(Inventory, Production, $rootScope, $scope, FormFactory, Auth) {
    var vm = this;

    $rootScope.$watch('hub', function(newValue, oldValue) {
      vm.finishContainerId = $rootScope.hub.machine ? $rootScope.hub.machine.finish_container : $rootScope.hub.finish_container;
    }, true);

    var getData = function() {
      $scope.$watch(angular.bind(vm, function() {
        return vm.finishContainerId;
      }), function(newValue, oldValue) {
        if (vm.finishContainerId && Auth.isLoggedIn()) {
          getFinishContainer();


          $scope.$on('inventory:created', function(event, data) {
            if (data.data.container === vm.finishContainerId) {
              getFinishContainer();

              // var foundBundle = _.find(vm.finishContainer, {
              //   id: data.id
              // });
              // if (!foundBundle) {
              //   vm.finishContainer.push(data.data);
              // } else {
              //   FormFactory.merge(data.data, vm.finishContainer);
              // }
            }
          });

          $scope.$on('inventory:updated', function(event, data) {
            var foundBundle = _.find(vm.finishContainer, {
              id: data.id
            });
            if (foundBundle) {
              FormFactory.merge(data.data, vm.finishContainer);
            }
          });

          $scope.$on('inventory:destroyed', function(event, data) {
            _.remove(vm.finishContainer, {
              id: data.id
            });
          });

        }
      });
    };

    if (Auth.isLoggedIn()) {
      getData();
    } else {
      var userWatch = $rootScope.$on('USER_CHANGED', function(event, data) {
        if (Auth.isLoggedIn()) {
          getData();
          userWatch();
        }
      });
    }


    function getFinishContainer() {
      if (_.isUndefined(vm.finishContainerId)) {
        vm.finishContainer = [];
        return null;
      }
      return Inventory.list({
        sort: 'createdAt',
        // limit: 3,
        where: {
          container: vm.finishContainerId
        },
        populate: ['unit', 'updator']
      }).$promise.then(function(result) {
        vm.finishContainer = result.result;
      });
    }

  }
})();
