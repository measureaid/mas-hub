/* globals $, Notification*/
(function() {
  'use strict';

  angular
    .module('masApp.hub')
    .service('hubService', hubService);

  function hubService(masResource) {
    var S = masResource('hub', {
      read: {
        method: 'GET',
        url: '/hub/:id'
      },
      status: {
        method: 'GET',
        isArray: true,
        url: '/hub/status'
      },
      working: {
        method: 'PUT',
        url: '/hub/working'
      },
      assign: {
        method: 'PUT',
        url: '/hub/assign'
      },
      unassign: {
        method: 'PUT',
        url: '/hub/unassign'
      },
      flush: {
        method: 'PUT',
        url: '/hub/flush'
      },
      print: {
        method: 'POST',
        url: '/print/bundle'
      },
      inject: {
        method: 'PUT',
        url: '/hub/inject'
      },
      defect: {
        method: 'PUT',
        url: '/hub/defect'
      },
      collect: {
        method: 'PUT',
        url: '/hub/collect'
      },
      start: {
        method: 'PUT',
        url: '/hub/production/start'
      },
      end: {
        method: 'PUT',
        url: '/hub/production/end'
      },
      endOnly: {
        method: 'PUT',
        url: '/hub/production/endOnly'
      },
      package: {
        method: 'PUT',
        url: '/hub/production/packageSimple'
      },
      containerCapacity: {
        method: 'GET',
        url: '/hub/containerCapacity'
      },
      packageUnit: {
        method: 'GET',
        url: '/hub/packageUnit'
      }


    });

    return S;

  }
})();