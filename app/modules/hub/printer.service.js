/* globals $, Notification*/
(function () {
	'use strict';

	angular
		.module('masApp.hub')
		.service('printerService', hubService);

	function hubService($rootScope, $resource, BASE_URL, toastr) {

		var S = $resource(BASE_URL + ':model/:modelId/:action/:actionId/', {}, {
			// tempResource
			bundle: {
				method: 'POST',
				params: {
					action: 'print',
					actionId: 'bundle'
				}
			},
		});

		return S;

	}
})();
