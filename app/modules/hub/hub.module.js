(function() {
  'use strict';

  angular
    .module('masApp.hub', [
      'ui.router'
    ])

    .config(function($stateProvider, ACCESS_LEVELS) {
      //자원
      $stateProvider
        .state('hub', {
          data: {
            access: 'user'
          },
          abstract: true,
          url: '/hub',
          templateUrl: 'views/hub/index.html',
        });
      //자원 리스트
      // .state('hub.receive', {
      //   url: '/receive/',
      //   views: {
      //     'hub': {
      //       templateUrl: 'modules/hub/receive/receive.html',
      //       controller: 'hubReceiveController',
      //       controllerAs: 'vm'
      //     }
      //   },
      // })

    });
})();