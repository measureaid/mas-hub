(function() {
  'use strict';

  angular
    .module('masApp.stock', [
      'ui.router',
    ]);
})();