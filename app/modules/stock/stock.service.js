/* globals io */
(function() {
  'use strict';

  angular
    .module('masApp.stock')
    .service('Stock', stockService);

  stockService.$inject = ['masResource'];

  function stockService(masResource) {
    // console.log('STOCK SERVICE LOADED')
    var S = masResource('stock', {
      // Container
      list: {
        method: 'GET',
      },
      read: {
        method: 'GET',
      },
      log: {
        method: 'GET',
        url: '/stock/log'
      },
    });
    return S;
  }
})();