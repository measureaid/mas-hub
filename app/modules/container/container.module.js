(function() {
  'use strict';

  angular
    .module('masApp.container', [
      'ui.router',
    ]);
})();