(function () {
	'use strict';

	angular
		.module('masApp.container')
		.service('Container', containerService);

	containerService.$inject = ['masResource'];

	function containerService(masResource) {
		var S = masResource('container', {
			// Container
			list: {
				method: 'GET',
			},
			create: {
				method: 'POST',
			},
			read: {
				method: 'GET',
				cache: true
			},
			hierarchical: {
				method: 'GET',
				url: '/container/:id/hierarchical',
				// isArray: true
			},
			update: {
				method: 'PUT',
			},
			delete: {
				method: 'DELETE',
				isArray: true,
			},
			navigator: {
				method: 'GET',
				url: '/container/:id/nav'
			},
			flush: {
				method: 'PUT',
				url: '/container/:id/flush'
			},

			clean: {
				method: 'PUT',
				url: '/container/:id/clean'
			},

			trace: {
				method: 'GET',
				isArray: true,
				url: '/container/:id/trace'
			},
			listInventory: {
				method: 'GET',
				url: '/container/:id/inventory'
			},
			readToRoot: {
				method: 'GET',
				isArray: true,
				url: '/container/:id/toroot'
			},
		});
		return S;
	}
})();
