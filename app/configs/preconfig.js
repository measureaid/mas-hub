/****** GLOBAL AREA *********/
var _DEBUG = true;
if (_DEBUG === false) {
  if (typeof console !== 'undefined') {
    console.log = function() {};
    console.debug = function() {};
    console.info = function() {};
  }
} else {
  if (typeof console === 'undefined') {
    var f = function() {};
    window.console = {
      log: f,
      info: f,
      warn: f,
      debug: f,
      error: f
    };
  }
}



var authConfig = {
  roles: [
    'public', //공개
    'customer', //일반고객
    'partner', //협력업체
    'partnerAdmin', //협력업체 관리자
    'user', //유저
    'wmsadmin', //창고관리자
    'accountant', //회계담당자
    'hq', //본사
    'director', //이사진
    'site', //현장 담당자
    'siteAdmin', //현장 관리자
    'admin', //관리자
    'superAdmin' //슈퍼 관리자 - 메져에이드
  ],
  accessLevels: {
    'public': '*',
    'external': ['partner', 'customer', 'site', 'siteAdmin', 'director', 'hq', 'admin', 'superAdmin'],
    'anon': ['public'],
    'login': ['customer', 'partner', 'partnerAdmin', 'user', 'wmsadmin', 'accountant', 'hq', 'director', 'site', 'siteAdmin', 'admin', 'superAdmin'],
    'customer': ['customer', 'site', 'admin', 'superAdmin'],
    'partnerOnly': ['partner', 'superAdmin'],
    'partner': ['partner', 'partnerAdmin', 'hq', 'admin', 'user', 'superAdmin'],
    'partnerAdmin': ['partnerAdmin', 'hq', 'admin', 'superAdmin', 'user', 'director'],
    'user': ['user', 'hq', 'wmsadmin', 'accountant', 'director', 'admin', 'superAdmin'],
    'wmsadmin': ['wmsadmin', 'hq', 'admin', 'superAdmin', 'director'], //입/출고 재고변동 관련
    'hq': ['hq', 'admin', 'superAdmin', 'director'], //생산계획, 몰드 등 변동, 재고도 권한 있음
    'accountant': ['accountant', 'director', 'admin', 'superAdmin'], //금액/회계 관련
    'director': ['director', 'admin', 'superAdmin'], //임원진
    'site': ['site', 'siteAdmin', 'admin', 'superAdmin'],
    'siteAdmin': ['siteAdmin', 'admin', 'superAdmin'],
    'admin': ['admin', 'superAdmin'],
    'superAdmin': ['superAdmin']
  }
};
var global = {};

global.BASE_URL = 'http://localhost:1337/';


/****** END OF GLOBAL AREA *********/