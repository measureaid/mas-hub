(function() {
  'use strict';
  /*jshint bitwise: false*/

  var authConfig = {
    roles: [
      'public', //공개
      'customer', //일반고객
      'partner', //협력업체
      'partnerAdmin', //협력업체 관리자
      'user', //유저
      'wmsadmin', //창고관리자
      'accountant', //회계담당자
      'hq', //본사
      'director', //이사진
      'site', //현장 담당자
      'siteAdmin', //현장 관리자
      'admin', //관리자
      'superAdmin' //슈퍼 관리자 - 메져에이드
    ],
    accessLevels: {
      'public': '*',
      'external': ['partner', 'customer', 'site'],
      'anon': ['public'],
      'login': ['customer', 'partner', 'partnerAdmin', 'user', 'wmsadmin', 'accountant', 'hq', 'director', 'site', 'siteAdmin', 'admin', 'superAdmin'],
      'customer': ['customer', 'site', 'admin', 'superAdmin'],
      'partnerOnly': ['partner'],
      'partner': ['partner', 'partnerAdmin', 'hq', 'admin', 'user'],
      'partnerAdmin': ['partnerAdmin', 'hq', 'admin', 'superAdmin', 'user'],
      'user': ['user', 'hq', 'wmsadmin', 'accountant', 'director', 'admin', 'superAdmin'],
      'wmsadmin': ['wmsadmin', 'hq', 'admin', 'superAdmin'], //입/출고 재고변동 관련
      'hq': ['hq', 'admin', 'superAdmin'], //생산계획, 몰드 등 변동, 재고도 권한 있음
      'accountant': ['accountant', 'director', 'admin', 'superAdmin'], //금액/회계 관련
      'director': ['director', 'admin', 'superAdmin'], //임원진
      'site': ['site', 'siteAdmin', 'admin', 'superAdmin'],
      'siteAdmin': ['siteAdmin', 'admin', 'superAdmin'],
      'admin': ['admin', 'superAdmin'],
      'superAdmin': ['superAdmin']
    }
  };


  var buildAccessLevels = function buildAccessLevels(accessLevelDeclarations) {
    var result = {};
    var userRoles = {};
    var bitMask = '01';

    var resultBitMask = '';
    for (var role in authConfig.roles) {
      var intCode = parseInt(bitMask, 2);
      userRoles[authConfig.roles[role]] = {
        bitMask: intCode,
        title: authConfig.roles[role]
      };
      bitMask = (intCode << 1).toString(2);
    }


    for (var level in accessLevelDeclarations) {
      resultBitMask = '';

      if (typeof accessLevelDeclarations[level] === 'string') {
        if (accessLevelDeclarations[level] === '*') {

          for (role in userRoles) {
            resultBitMask += '1';
          }
          resultBitMask = parseInt(resultBitMask, 2);

        } else {
          console.error('Access Control Error: Could not parse \'' + accessLevelDeclarations[level] + '\' as access definition for level \'' + level + '\'');
        }
      } else {
        for (role in accessLevelDeclarations[level]) {
          // resultBitMask = 0;
          if (userRoles.hasOwnProperty(accessLevelDeclarations[level][role])) {
            // console.log(resultBitMask, userRoles[accessLevelDeclarations[level][role]].bitMask, resultBitMask | userRoles[accessLevelDeclarations[level][role]].bitMask);

            resultBitMask = resultBitMask | userRoles[accessLevelDeclarations[level][role]].bitMask;
          } else {
            console.error('Access Control Error: Could not find role \'' + accessLevelDeclarations[level][role] + '\' in registered roles while building access for \'' + level + '\'');
          }
        }
      }
      result[level] = {
        bitMask: resultBitMask
      };
    }
    // console.log(result);
    return result;
  };


  angular
    .module('mas.user')
    .constant('AUTH_PREFIX', 'ma')
    .constant('AUTH_SERVER_CONFIG', {
      'register': {
        method: 'POST',
        url: '/auth/local/register'
      },
      'login': {
        method: 'POST',
        url: '/auth/local'
      },
      'loginByBarcode': {
        method: 'POST',
        url: '/auth/barcode'
      },
      'logout': {
        method: 'GET',
        url: '/auth/logout'
      },

      'findpassword': {
        method: 'POST',
        url: '/user/findpassword'
      },
      'resetpassword': {
        method: 'POST',
        url: '/auth/local/reset'
      },
      'checkUser': {
        method: 'GET',
        url: '/user/check'
      }
    })
    //유저롤
    .constant('USER_ROLES', (function() {
      var bitMask = '01';
      var userRoles = {};

      for (var role in authConfig.roles) {
        var intCode = parseInt(bitMask, 2);
        userRoles[authConfig.roles[role]] = {
          bitMask: intCode,
          title: authConfig.roles[role]
        };
        bitMask = (intCode << 1).toString(2);
      }
      // console.log(userRoles);
      return userRoles;
    })())

    ///엑세스 레벨
    .constant('AUTH_STATE_LOGIN', 'public.signin')
    .constant('AUTH_STATE_HOME', 'public.home')
    .constant('ACCESS_LEVELS', buildAccessLevels(authConfig.accessLevels));
})();