(function() {
  'use strict';

  angular
    .module('masApp')
    /******************* local storage prefix *************/
    .constant('PREFIX', 'mas')
    /********************* backend url ********************/
    .constant('BASE_URL', global.BASE_URL)
    .constant('IMG_BASE_URL', '/api/')
    .constant('MAS_CONFIG', {
      title: 'MeasureAid'
    })
    .config(
      function($stateProvider, ACCESS_LEVELS) {

        $stateProvider
          .state('public', {
            abstract: true,
            data: {
              access: 'public'
            },
            url: '',
            template: '<ui-view/>',
          })
          .state('public.disconnected', {
            url: '/disconnected/',
            templateUrl: 'views/common/disconnected.html',
            controller: 'disconnectedController',
            controllerAs: 'vm'
          })

          .state('public.signin', {
            url: '/login/',
            templateUrl: 'views/user/signin.html',
            controller: 'signinFormController',
            controllerAs: 'vm'
          })

          .state('public.home', {
            url: '/home/',
            templateUrl: 'views/home/home.html',
            controller: 'HomeController',
            controllerAs: 'vm'
          })

          .state('user', {
            abstract: true,
            data: {
              access: 'user'
            },
            url: '/user',
            template: '<ui-view/>',
          })
          .state('anon', {
            abstract: true,
            data: {
              access: 'anon'
            },
            url: '/anon',
            template: '<ui-view/>',
          })
          .state('print', {
            data: {
              access: 'user'
            },
            url: '/print',
            templateUrl: 'modules/bundle/print/bundle-print.html',
            controller: 'HubBundlePrintController',
            controllerAs: 'vm'
          })

          .state('user.logout', {
            url: '/logout/',
            // templateUrl: 'views/user/home.html',
            controller: function($scope, $rootScope, $state, Auth) {

              Auth.logout(function(res) {
                // console.log('로그아웃', res);
                $state.go('public.signin', {
                  reload: true
                });
              });
            }
          });

      }
    )


    // 라우팅 설정
    .config(function($stateProvider, $urlRouterProvider, $locationProvider, $httpProvider) {
      $urlRouterProvider.otherwise('/');

      $urlRouterProvider.rule(function($injector, $location) {
        if ($location.protocol() === 'file') {
          return;
        }

        var path = $location.path(),
          // Note: misnomer. This returns a query object, not a search string
          search = $location.search(),
          params;

        // check to see if the path already ends in '/'
        if (path[path.length - 1] === '/') {
          return;
        }

        // If there was no search string / query params, return with a `/`
        if (Object.keys(search).length === 0) {
          return path + '/';
        }

        // Otherwise build the search string and return a `/?` prefix
        params = [];
        angular.forEach(search, function(v, k) {
          params.push(k + '=' + v);
        });
        return path + '/?' + params.join('&');
      });

      // $locationProvider.html5Mode(true);
      // $locationProvider.hashPrefix('!');

      $httpProvider.interceptors.push(function($q, $location, $window, $injector) {

        return {
          'responseError': function(response) {
            if (response.status === 401 || response.status === 403) {
              $location.path('/login/');
            } else if (response.status === 508) {
              var Auth = $injector.get('Auth');
              Auth.logout(function(res) {
                // console.log('로그아웃', res);
                $window.location.reload();
              });
            }
            return $q.reject(response);
          }
        };
      });
    });
})();