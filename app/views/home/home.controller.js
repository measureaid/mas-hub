(function() {
  'use strict';

  angular
    .module('masApp')
    .controller('HomeController', HomeController);

  /** @ngInject */
  function HomeController($rootScope, $window, toastr, hubService) {
    var vm = this;

    vm.start = function() {
      $rootScope.$broadcast('production:start');
    };

    vm.end = function() {
      $rootScope.$broadcast('production:finish');
      $window.location.reload();
    };

    vm.endOnly = function() {
      var hub = $rootScope.hub;
      if (!hub.operation) {
        return toastr.error('공정이 할당되어 있지 않습니다.', '생산 시작 실패');
      }
      if (!hub.targetResource) {
        return toastr.error('목표 생산 자원이 설정되어 있지 않습니다.', '생산 시작 실패');
      }

      return hubService.end({
        // resourceId: hub.targetResource,
        // operationId: hub.operation
      }).$promise.then(function(success) {

      }, function(err) {
        toastr.error(err.body.message, '생산 종료 실패');
      });
    };

    vm.simpleCollect = function() {
      return hubService.collect({
        simple: true
      });
    }

    vm.forced = function() {
      $rootScope.$broadcast('recommand:force', {
        barcode: $rootScope.error.barcode,
        force: true
      });
    }



  }

}());