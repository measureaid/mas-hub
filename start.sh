#!/bin/bash
until $(curl --output /dev/null --silent --head --fail http://bitbucket.org); do
    printf '.'
    sleep 5
done

sudo su - pi -c "cd /home/pi/mas-hub; git pull; npm install; bower install; npm start"