const debug = /--debug/.test(process.argv[2]);
var nconf = require('nconf');
var _ = require('lodash');

var count = 0;
var mainWindow = null;

var fs = require('fs');
const path = require('path');
const electron = require('electron');
const autoUpdater = require('./auto-updater');
const BrowserWindow = electron.BrowserWindow;
const app = electron.app;

const ipc = require('electron').ipcMain;
var exec = require('child_process').exec;
var serialConfig = require('./serialConfig');

nconf.argv()
  .env()
  .file({
    file: 'config.json'
  });

//맥주소 관련 처리
var mac = '';
var mac_preset = false;
if (process.argv.length > 2 && process.argv[3]) {
  mac = process.argv[3];
  mac_preset = true;
}
require('macaddress').one(function (err, macAddress) {
  if (!mac_preset) {
    mac = macAddress;
  }
});

var pin1, pin2;
if (!nconf.get('sensor_type')) {
  nconf.set('sensor_type', 'single');
}
if (!nconf.get('pin1')) {
  nconf.set('pin1', 4);
  pin1 = nconf.get('pin1');
} else {
  pin1 = nconf.get('pin1');
}
if (!nconf.get('pin2')) {
  nconf.set('pin2', 5);
  pin2 = nconf.get('pin2');
} else {
  pin2 = nconf.get('pin2');
}

if (!nconf.get('count')) {
  nconf.set('count', 0);
  count = nconf.get('count');
} else {
  count = nconf.get('count');
}

//
// Save the configuration object to disk
//
nconf.save(function (err) {
  fs.readFile('config.json', function (err, data) {
    // console.dir(JSON.parse(data.toString()))
  });
});


ipc.on('reboot', function (event, arg) {
  exec("sudo reboot", function (error, stdout, stderr) {
    console.log(stdout);
  });
});

ipc.on('remote', function (event, port) {
  var spawn = require('child_process').spawn;
  console.log(port, 'remote');
  if (ssh) {
    console.log('이미 연결되었습니다.');
    return;
  }
  var pwd = spawn('pwd');
  pwd.stdout.on('data', function (data) {
    console.log(data.toString());
  });

  var ssh = spawn('ssh', [
    '-tt',
    '-nNT',
    '-i', 'keys/id_rsa',
    // '-S', '/tmp/TEMP_SSH_CTL_FILE',
    // '-L', 'LOCAL_PORT:REMOTE_HOST:REMOTE_HOST_PORT',
    '-R', port + ':localhost:5900',
    'vnc@measureaid.com',
    '-p', '2121',
    '-o', 'StrictHostKeyChecking=no'
  ]);

  ssh.stdout.on('data', function (data) {
    console.log(data.toString());
  });

  ssh.stderr.on('data', function (data) {
    console.log('stderr: ' + data);
  });

  process.on('exit', () => {
    ssh.kill();
  });
});

ipc.on('shutdown', function (event, arg) {
  exec("sudo shutdown -h now", function (error, stdout, stderr) {
    console.log(stdout);
  });
});

ipc.on('mac', function (event, arg) {
  if (mac) {
    console.log('MAC', mac);
  }
  event.returnValue = mac;
});

ipc.on('exit', function (event, arg) {
  event.returnValue = app.quit();
});


if (process.platform === 'linux') {
  var wpi = require('wiringpi-node');
  var moldOpenPin = pin1;
  var moldClosePin = pin2;
  var openState;

  wpi.setup('wpi');

  wpi.pinMode(moldOpenPin, wpi.INPUT);
  wpi.pullUpDnControl(moldOpenPin, wpi.PUD_DOWN);
  var closeState = wpi.digitalRead(moldOpenPin);

  wpi.pinMode(moldClosePin, wpi.INPUT);
  wpi.pullUpDnControl(moldClosePin, wpi.PUD_DOWN);
  var closeState = wpi.digitalRead(moldClosePin);
}


/* Serial device */
var serialDevice = require('./lib/serialDevice');
var sd = new serialDevice();
sd.addDeviceList(serialConfig.device[0]);
sd.setBaudRate(serialConfig.baudRate);
sd.setCheckInterval(serialConfig.interval);
sd.setReadlineParser(function (data) {
  /* Remove needless character and spaces */
  console.log(data);
  var parsedData = data.replace(/ /g, '');
  mainWindow.webContents.send('barcode', parsedData);
});
sd.connectDevice(function (port) {
  sd.startAutoReconnect();
});



function initialize() {
  // var shouldQuit = makeSingleInstance();
  // if (shouldQuit) return app.quit();

  // loadDemos();

  function createWindow() {

    if (!nconf.get('sshkey')) {
      var keygen = require('ssh-keygen');
      var fs = require('fs');

      var location = __dirname + '/keys/ssh_key';
      var comment = 'vnc@measureaid.com';
      var password = false; // false and undefined will convert to an empty pw

      keygen({
        location: location,
        comment: comment,
        password: password,
        read: true
      }, function (err, out) {
        if (err) return console.log('Something went wrong: ' + err);

        nconf.set('sshkey', out.pubKey);
        realInitialize();
      });
    } else {
      realInitialize();
      // pin1 = nconf.get('pin1');
    }


  }

  function realInitialize() {

    var windowOptions = {
      width: 800,
      height: 480,
      // debug: false,
      'fullscreen': true,
      'frame': false,
      'kiosk': true,
      webPreferences: {
        partition: 'persist:' + mac
      },
    };
    if (process.platform === 'linux') {
      windowOptions.icon = path.join(__dirname, '/assets/app-icon/png/512.png');
    }

    // var session = electron.session;
    // var ses = session.fromPartition('persist:name');

    mainWindow = new BrowserWindow(windowOptions);
    var mainPage = path.join('file://', __dirname, 'app', 'index.html');
    mainWindow.loadURL(mainPage);

    // Launch fullscreen with DevTools open, usage: npm run debug
    if (debug) {
      mainWindow.webContents.openDevTools();
      mainWindow.maximize();
    }
    var lastState = null;
    if (process.platform === 'linux') {
      setInterval(function () {
        var read = wpi.digitalRead(moldClosePin);
        if (read !== lastState) {
          if (read) {
            console.log('생산시작');
            // mainWindow.webContents.send('production:start');
          } else {
            if (!_.isNull(lastState)) {
              console.log('생산종료');
              mainWindow.webContents.send('production:endOnly');
            } else {
              console.log('생산종료(비정상)');
              mainWindow.webContents.send('production:endOnly');
            }
          }
        }
        lastState = read;
      }, 100);
    }

    mainWindow.on('closed', function () {
      mainWindow = null;
    });
  }

  app.on('ready', function () {
    createWindow();
    autoUpdater.initialize();
  });

  app.on('window-all-closed', function () {
    if (process.platform !== 'darwin') {
      app.quit();
    }
  });

  app.on('activate', function () {
    if (mainWindow === null) {
      createWindow();
    }
  });
}

// Make this app a single instance app.
//
// The main window will be restored and focused instead of a second window
// opened when a person attempts to launch a second instance.
//
// Returns true if the current version of the app should quit instead of
// launching.
function makeSingleInstance() {
  return app.makeSingleInstance(function () {
    if (mainWindow) {
      if (mainWindow.isMinimized()) mainWindow.restore();
      mainWindow.focus();
    }
  });
}

initialize();